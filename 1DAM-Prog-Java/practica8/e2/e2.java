/** 
 * Práctica Programación. - e2.java
 * 
 * Crea y carga con datos de prueba un ArrayList de diez estudiantes utilizando la clase Estudiante
 * y ordenarlos por según la nota de evaluación.
 * 
 * Se debe implementar en la clase Estudiante la interfaz java.lang.Comparable.
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/05/09
 * @author: mfp
 */
package e2;

import java.util.ArrayList;

import e2.Estudiante;

public class e2 {

	public static void main(String[] args) {

		ArrayList<Estudiante> estudiantes = new ArrayList<Estudiante>();
		
		for (int i = 0; i < 10; i++) {
			estudiantes.add(new Estudiante("Estudiante " + i, "Anonimo " + i, Math.rint(Math.random()*100)/10));
		}
		
		System.out.println("Lista de estudiantes sin ordenar");
		for (Estudiante estudiante : estudiantes) {
			System.out.println(estudiante);
		}
		
		estudiantes.sort(null);

		System.out.println("\nLista de estudiantes ordenada");
		for (Estudiante estudiante : estudiantes) {
			System.out.println(estudiante);
		}
	}

}

