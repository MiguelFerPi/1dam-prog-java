/** 
 * Práctica Programación.8 - Ejercicio 1 - Estudiante.java
 * 
 * Define en Java:
 * 
 * Una clase Persona con los atributos nombre y apellidos.
 * 
 * Una clase Estudiante, que derive de Persona, con un atributo llamado evaluacion.
 * 
 * Definir una clase Trabajador, que derive Persona, con los atributos salario y horasTrabajadas. Además dispone un método calcularSueldo(), que se calcula el salario, en base a a un precio y las horas trabajadas.
 * En todas las clases definidas, escribe constructores, métodos de acceso a los atributos y sobreescribe el método toString() de manera adecuada.
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/05/09
 * @author: mfp
 */
package e2;

public class Estudiante extends Persona implements Comparable {

	protected double evaluacion;

	/**
	 * Constructor principal
	 */
	public Estudiante(String nombre, String apellidos, double evaluacion) {
		super(nombre, apellidos);
		setEvaluacion(evaluacion);
	}

	/**
	 * Constructor por defecto
	 */
	public Estudiante() {
		this("Alumno", "Alumno", 0.0);

	}

	/**
	 * Constructor copia
	 */
	public Estudiante(Estudiante estudiante) {
		this(estudiante.nombre, estudiante.apellidos, estudiante.evaluacion);
	}

	/**
	 * Devuelve la evaluación de Estudiante
	 * 
	 * @return evaluacion
	 */
	public double getEvaluacion() {
		return evaluacion;
	}

	/**
	 * Establece la evaluación de Estudiante
	 * 
	 * @param evaluacion
	 */
	public void setEvaluacion(double evaluacion) {
		this.evaluacion = evaluacion;
	}

	/**
	 * Devuelve los datos del Estudiante a como String.
	 */
	@Override
	public String toString() {
		return String.format("%s %s: %s", nombre, apellidos, evaluacion);
	}

	/**
	 * Compara esta evaluación con la introducida y devuelve -1 si esta nota es
	 * inferior, 0 si es igual, y 1 si es mayor.
	 *
	 * @param Estudiante
	 * @return -1 si es inferior
	 * @return 0 si es igual
	 * @return 1 si es mayor
	 */
	@Override
	public int compareTo(Object estudiante) {
		Estudiante estudianteComparado = (Estudiante) estudiante;
		if (this.evaluacion < estudianteComparado.evaluacion) {
			return -1;
		}
		if (this.evaluacion > estudianteComparado.evaluacion) {
			return 1;
		}
		return 0;
	}

}
