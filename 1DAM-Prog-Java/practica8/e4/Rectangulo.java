/** 
 * Práctica Programación. - Triangulo.java
 * 
 * Define en Java:
 * 
 * Una clase FiguraGeometrica, con los atributos alto y ancho.
 * Además dispone de un método abstracto calcularArea().
 * 
 * Dos clases llamadas Triangulo y Rectangulo derivadas de FiguraGeometrica.
 * Las dos clases deben hacer implementar respectivamente el método calcularArea().
 * 
 * Una clase Circulo derivada de FiguraGeometrica, con un constructor para inicializar 
 * los atributos heredados con el valor del radio e implementar método para calcular el área.
 * 
 * Deben seguirse los principios y estilo del código limpio.
 *  
 * @version: 1.0 - 2019/05/10
 * @author: mfp
 */
package e4;

public class Rectangulo extends FiguraGeometrica {

	@Override
	public double calcularArea() {
		return alto * ancho;
	}

}
