/** 
 * Práctica Programación.8 - Ejercicio 1 - Persona.java
 * 
 * Define en Java:
 * 
 * Una clase Persona con los atributos nombre y apellidos.
 * 
 * Una clase Trabajador, que derive de Persona, con un atributo llamado salario.
 * 
 * Definir una clase Trabajador, que derive Persona, con los atributos salario y horasTrabajadas.
 * Además dispone un método calcularSueldo(), que se calcula el salario, en base a a un precio y las horas trabajadas.
 * 
 * En todas las clases definidas, escribe constructores, métodos de acceso a los atributos y sobreescribe el método toString() de manera adecuada.
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/05/09
 * @author: mfp
 */
package e3;

public class Persona {

	// Atributos
	protected String nombre;
	protected String apellidos;

	/**
	 * Constructor principal
	 */
	public Persona(String nombre, String apellidos) {
		setNombre(nombre);
		setApellidos(apellidos);
	}

	/**
	 * Constructor por defecto
	 */
	public Persona() {
		this("Pepito", "Peréz López");
	}

	/**
	 * Constructor copia
	 */
	public Persona(Persona psn) {
		this(psn.nombre, psn.apellidos);
	}

	/**
	 * Devuelve el nombre de la Persona.
	 * 
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el nombre de la Persona
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve los apellidos de la Persona
	 * 
	 * @return apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}

	/**
	 * Establece los apellidos de la Persona
	 * 
	 * @param apellidos
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	/**
	 * Devuelve el nombre y apellidos de la Persona como String.
	 */
	@Override
	public String toString() {
		return String.format("%s %s", nombre, apellidos);
	}

}
