/** 
 * Práctica Programación.8 - Ejercicio 1 - Trabajador.java
 * 
 * Define en Java:
 * 
 * Una clase Persona con los atributos nombre y apellidos.
 * 
 * Una clase Trabajador, que derive de Persona, con un atributo llamado salario.
 * 
 * Definir una clase Trabajador, que derive Persona, con los atributos salario y horasTrabajadas.
 * Además dispone un método calcularSueldo(), que se calcula el salario, en base a a un precio y las horas trabajadas.
 * 
 * En todas las clases definidas, escribe constructores, métodos de acceso a los atributos y sobreescribe el método toString() de manera adecuada.
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/05/09
 * @author: mfp
 */
package e3;

public class Trabajador extends Persona implements Comparable {

	final int PRECIO_HORA = 5;

	protected int salario;
	protected int horasTrabajadas;

	/**
	 * Constructor principal
	 */
	public Trabajador(String nombre, String apellidos, int horasTrabajadas) {
		super(nombre, apellidos);
		setHorasTrabajadas(horasTrabajadas);
		setSalario(calcularSueldo(horasTrabajadas));
	}

	/**
	 * Constructor por defecto
	 */
	public Trabajador() {
		this("Trabajador", "Trabajador", 0);

	}

	/**
	 * Constructor copia
	 */
	public Trabajador(Trabajador tbj) {
		this(tbj.nombre, tbj.apellidos, tbj.horasTrabajadas);
	}

	/**
	 * Calcula el salario en base a las horas trabajadas y el precio por hora
	 * establecido.
	 * 
	 * @param horasTrabajadas
	 * @return salario
	 */
	private int calcularSueldo(int horasTrabajadas) {
		return PRECIO_HORA * horasTrabajadas;
	}

	/**
	 * Devuelve el salario del Trabajador
	 * 
	 * @return salario
	 */
	public int getSalario() {
		return salario;
	}

	/**
	 * Establace el salario del Trabajador
	 * 
	 * @param salario
	 */
	public void setSalario(int salario) {
		this.salario = salario;
	}

	/**
	 * Devuelve las horas trabajadas del Trabajador
	 * 
	 * @return horasTrabajadas
	 */
	public int getHorasTrabajadas() {
		return horasTrabajadas;
	}

	/**
	 * Establece las horas trabajadas del Trabajador
	 * 
	 * @param horasTrabajadas
	 */
	public void setHorasTrabajadas(int horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	/**
	 * Devuelve los datos del Trabajador como String.
	 */
	@Override
	public String toString() {
		return String.format("%s %s: Salario: %s, Horas trabajadas: %s", nombre, apellidos, salario, horasTrabajadas);
	}

	/**
	 * Compara esta evaluación con la introducida y devuelve -1 si esta nota es
	 * inferior, 0 si es igual, y 1 si es mayor.
	 *
	 * @param Trabajador
	 * @return -1 si es inferior
	 * @return 0 si es igual
	 * @return 1 si es mayor
	 */
	@Override
	public int compareTo(Object trabajador) {
		Trabajador trabajadorComparado = (Trabajador) trabajador;
		if (this.salario < trabajadorComparado.salario) {
			return 1;
		}
		if (this.salario > trabajadorComparado.salario) {
			return -1;
		}
		return 0;
	}

}
