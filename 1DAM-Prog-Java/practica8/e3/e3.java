/** 
 * Práctica Programación. - e3.java
 * 
 * Crea y carga con datos de prueba un ArrayList de diez estudiantes utilizando la clase Trabajador
 * y ordenarlos por según la nota de evaluación.
 * 
 * Se debe implementar en la clase Trabajador la interfaz java.lang.Comparable.
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/05/09
 * @author: mfp
 */
package e3;

import java.util.ArrayList;

public class e3 {

	public static void main(String[] args) {

		ArrayList<Trabajador> trabajadores = new ArrayList<Trabajador>();
		
		for (int i = 0; i < 10; i++) {
			trabajadores.add(new Trabajador("Trabajador " + i, "Anonimo " + i, (int) Math.rint(Math.random()*100)));
		}
		
		System.out.println("Lista de trabajadores sin ordenar");
		for (Trabajador trabajador : trabajadores) {
			System.out.println(trabajador);
		}
		
		trabajadores.sort(null);

		System.out.println("\nLista de trabajadores ordenada");
		for (Trabajador trabajador : trabajadores) {
			System.out.println(trabajador);
		}
	}

}

