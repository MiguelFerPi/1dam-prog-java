/** 
 * Práctica Programación. - e5.java
 * 
 * Define en Java:
 * 
 * Una clase Humano con propiedades nombre y apellidos.
 * 
 * La clase Estudiante que hereda de Humano, que tiene el atributo listaNotas.
 * 
 * La clase Trabajador que hereda de Humano con los atributos sueldo y horasTrabajadas.
 * 
 * Implementa un método para calcular el salario por hora de cualquier trabajador. Escribe los constructores correspondientes y encapsula todos los atributos.
 * 
 * Deben seguirse los principios y estilo del código limpio.
 *
 *  Este no lo pedía el ejercicio pero para probarlo lo hice y ya lo dejo.
 *  
 * @version: 1.0 - 2019/05/14
 * @author: mfp
 */
package e5;

public class e5 {

	public static void main(String[] args) {

		double[] notas = new double[10];
		for (int i = 0; i < notas.length; i++) {
			notas[i] = Math.rint(Math.random()*100)/10;
		}
		
		Estudiante estudiante = new Estudiante("Pepito", "Grillo", notas);
		
		System.out.println(estudiante);
	}

}

