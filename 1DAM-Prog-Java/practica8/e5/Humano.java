/** 
 * Práctica Programación. - Humano.java
 * 
 * Define en Java:
 * 
 * Una clase Humano con propiedades nombre y apellidos.
 * 
 * La clase Estudiante que hereda de Humano, que tiene el atributo listaNotas.
 * 
 * La clase Trabajador que hereda de Humano con los atributos sueldo y horasTrabajadas.
 * 
 * Implementa un método para calcular el salario por hora de cualquier trabajador. Escribe los constructores correspondientes y encapsula todos los atributos.
 * 
 * Deben seguirse los principios y estilo del código limpio.
 *  
 * @version: 1.0 - 2019/05/14
 * @author: mfp
 */
package e5;

public class Humano {

	// Atributos
	protected String nombre;
	protected String apellidos;

	/**
	 * Constructor principal
	 */
	public Humano(String nombre, String apellidos) {
		setNombre(nombre);
		setApellidos(apellidos);
	}

	/**
	 * Constructor por defecto
	 */
	public Humano() {
		this("Pepito", "Peréz López");
	}

	/**
	 * Constructor copia
	 */
	public Humano(Humano hmn) {
		this(hmn.nombre, hmn.apellidos);
	}

	/**
	 * Devuelve el nombre de la Persona.
	 * 
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el nombre de la Persona
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve los apellidos de la Persona
	 * 
	 * @return apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}

	/**
	 * Establece los apellidos de la Persona
	 * 
	 * @param apellidos
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	/**
	 * Devuelve el nombre y apellidos del Humano como String.
	 */
	@Override
	public String toString() {
		return String.format("%s %s", nombre, apellidos);
	}

}
