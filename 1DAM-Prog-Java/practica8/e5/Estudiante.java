/** 
 * Práctica Programación.8 - Ejercicio 1 - Estudiante.java
 * 
 * Define en Java:
 * 
 * Una clase Humano con propiedades nombre y apellidos.
 * 
 * La clase Estudiante que hereda de Humano, que tiene el atributo listaNotas.
 * 
 * La clase Trabajador que hereda de Humano con los atributos sueldo y horasTrabajadas.
 * 
 * Implementa un método para calcular el salario por hora de cualquier trabajador. Escribe los constructores correspondientes y encapsula todos los atributos.
 * 
 * Deben seguirse los principios y estilo del código limpio.
 *  
 * @version: 1.0 - 2019/05/09
 * @author: mfp
 */
package e5;

public class Estudiante extends Humano {

	protected double[] listaNotas = new double[10];

	/**
	 * Constructor principal
	 */
	public Estudiante(String nombre, String apellidos, double[] listaNotas) {
		super(nombre, apellidos);
		setListaNotas(listaNotas);
	}

	/**
	 * Constructor por defecto
	 */
	public Estudiante() {
		this("Alumno", "Alumno", null);

	}

	/**
	 * Constructor copia
	 */
	public Estudiante(Estudiante estudiante) {
		this(estudiante.nombre, estudiante.apellidos, estudiante.listaNotas);
	}

	/**
	 * Devuelve la evaluación de Estudiante
	 * 
	 * @return listaNotas
	 */
	public double[] getListaNotas() {
		return listaNotas;
	}

	/**
	 * Establece la evaluación de Estudiante
	 * 
	 * @param listaNotas
	 */
	public void setListaNotas(double[] listaNotas) {
		if (listaNotas == null) {
			return;
		}
		this.listaNotas = listaNotas;
	}

	/**
	 * Devuelve el nombre y apellidos del Estudiante como String.
	 */
	@Override
	public String toString() {
		StringBuilder valorToString = new StringBuilder(super.toString());
		for (int i = 0; i < listaNotas.length; i++) {
			valorToString.append(String.format("\n\tNota %s: %s", i + 1, listaNotas[i]));
		}
		return valorToString.toString();
	}

}
