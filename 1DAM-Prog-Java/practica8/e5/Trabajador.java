/** 
 * Práctica Programación.8 - Ejercicio 1 - Trabajador.java
 * 
 * 
 * Define en Java:
 * 
 * Una clase Humano con propiedades nombre y apellidos.
 * 
 * La clase Estudiante que hereda de Humano, que tiene el atributo listaNotas.
 * 
 * La clase Trabajador que hereda de Humano con los atributos sueldo y horasTrabajadas.
 * 
 * Implementa un método para calcular el salario por hora de cualquier trabajador. Escribe los constructores correspondientes y encapsula todos los atributos.
 * 
 * Deben seguirse los principios y estilo del código limpio.
 *  
 * @version: 1.0 - 2019/05/09
 * @author: mfp
 */
package e5;

public class Trabajador extends Humano {

	final int PRECIO_HORA = 5;

	protected int salario;
	protected int horasTrabajadas;

	/**
	 * Constructor principal
	 */
	public Trabajador(String nombre, String apellidos, int horasTrabajadas) {
		super(nombre, apellidos);
		this.horasTrabajadas = horasTrabajadas;
		setSalario(calcularSueldo(horasTrabajadas));
	}

	/**
	 * Constructor por defecto
	 */
	public Trabajador() {
		this("Trabajador", "Trabajador", 0);

	}

	/**
	 * Constructor copia
	 */
	public Trabajador(Trabajador tbj) {
		this(tbj.nombre, tbj.apellidos, tbj.horasTrabajadas);
	}

	/**
	 * Calcula el salario en base a las horas trabajadas y el precio por hora
	 * establecido.
	 * 
	 * @param horasTrabajadas
	 * @return salario
	 */
	private int calcularSueldo(int horasTrabajadas) {
		return PRECIO_HORA * horasTrabajadas;
	}

	/**
	 * Devuelve el salario del Trabajador
	 * 
	 * @return salario
	 */
	public int getSalario() {
		return salario;
	}

	/**
	 * Establace el salario del Trabajador
	 * 
	 * @param salario
	 */
	public void setSalario(int salario) {
		this.salario = salario;
	}

	/**
	 * Devuelve las horas trabajadas del Trabajador
	 * 
	 * @return horasTrabajadas
	 */
	public int getHorasTrabajadas() {
		return horasTrabajadas;
	}

	/**
	 * Establece las horas trabajadas del Trabajador
	 * 
	 * @param horasTrabajadas
	 */
	public void setHorasTrabajadas(int horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

}
