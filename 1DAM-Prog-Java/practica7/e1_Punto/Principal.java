/** 
 * Práctica Programación. - Principal.java
 * 
 * Define la clase Punto representada por sus coordenadas x e y (números reales).
 * Prueba la creación de objetos de la clase instanciando un nuevo punto en la clase Principal, que contiene el método main().
 * Asigna valores a las coordenadas de los puntos creados.
 * Muestra por pantalla las coordenadas de cada punto.
 * Modificar alguna de sus coordenadas accediendo directamente al atributo correspondiente y vuelve a mostrar los puntos por pantalla.
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/04/04
 * @author: mfp
 */

package e1_Punto;

class Principal {

	public static void main(String[] args) {
		Punto lugar = new Punto();

		lugar.x = 50;
		lugar.y = -100;
		System.out.println(String.format("El punto tiene como coordenadas X: %d, Y:%d.", lugar.x, lugar.y));

		lugar.x = 75;
		System.out.println(String.format("El punto tiene ahora como coordenadas X: %d, Y:%d.", lugar.x, lugar.y));
	}
}
