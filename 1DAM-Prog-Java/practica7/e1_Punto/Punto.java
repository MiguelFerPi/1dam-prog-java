/** 
 * Práctica Programación. - Punto.java
 * 
 * Define la clase Punto representada por sus coordenadas x e y (números reales).
 * Prueba la creación de objetos de la clase instanciando un nuevo punto en la clase Principal, que contiene el método main().
 * Asigna valores a las coordenadas de los puntos creados.
 * Muestra por pantalla las coordenadas de cada punto.
 * Modificar alguna de sus coordenadas accediendo directamente al atributo correspondiente y vuelve a mostrar los puntos por pantalla.
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/04/04
 * @author: mfp
 */

package e1_Punto;

class Punto {

	int x;
	int y;
	
}

