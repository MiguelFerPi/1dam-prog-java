/** 
 * Práctica Programación. - Manzana.java
 * 
 * Identifica los datos que consideras adecuados para representar el estado de los siguientes objetos teniendo en cuenta el contexto en el que se vayan a utilizar:
 * 
 * Una manzana (de las que se venden en un mercado)
 * 
 * Crea las correspondientes clases en Java.
 * Define los constructores típicos de cada una de las clases. Implementa los correspondientes métodos para el acceso y la modificación del estado de los objetos (los métodos get y set).
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/04/04
 * @author: mfp
 */
package e2_Clases;

class Manzana {

	// Atributos
	String categoria;
	String color;
	int precio;
	int peso;

	/**
	 * Constructor Principal
	 */
	Manzana(String categoria, String color, int precio, int peso) {
		this.setCategoria(categoria);
		this.setColor(color);
		this.setPrecio(precio);
		this.setPeso(peso);
	}

	/**
	 * Constructor copia
	 */
	Manzana(Manzana datos) {
		this(datos.categoria, datos.color, datos.precio, datos.peso);
	}

	/**
	 * Constructor por defecto
	 */
	Manzana() {
		this("Golden", "Rojo", 1, 200);
	}

	/**
	 * Getter del atributo categoria
	 * 
	 * @return String
	 */
	String getCategoria() {
		return categoria;
	}

	/**
	 * Setter del atributo categoria
	 * 
	 * @param categoria
	 */
	void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	/**
	 * Getter del atributo color
	 * 
	 * @return String
	 */
	String getColor() {
		return color;
	}

	/**
	 * Setter del atributo color
	 * 
	 * @param color
	 */
	void setColor(String color) {
		this.color = color;
	}

	/**
	 * Getter del atributo precio
	 * 
	 * @return int
	 */
	int getPrecio() {
		return precio;
	}

	/**
	 * Setter del atributo precio
	 * 
	 * @param precio
	 */
	void setPrecio(int precio) {
		this.precio = precio;
	}

	/**
	 * Getter del atributo peso
	 * 
	 * @return int
	 */
	int getPeso() {
		return peso;
	}

	/**
	 * Setter del atributo peso
	 * 
	 * @param peso
	 */
	void setPeso(int peso) {
		this.peso = peso;
	}

}
