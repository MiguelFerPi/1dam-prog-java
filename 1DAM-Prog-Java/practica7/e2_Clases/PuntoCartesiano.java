/** 
 * Práctica Programación. - PuntoCartesiano.java
 * 
 * Identifica los datos que consideras adecuados para representar el estado de los siguientes objetos teniendo en cuenta el contexto en el que se vayan a utilizar:
 * 
 * Un punto en un espacio cartesiano
 * 
 * Crea las correspondientes clases en Java.
 * Define los constructores típicos de cada una de las clases. Implementa los correspondientes métodos para el acceso y la modificación del estado de los objetos (los métodos get y set).
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/04/04
 * @author: mfp
 */
package e2_Clases;

class PuntoCartesiano {

	// Atributos
	int x;
	int y;
	int z;

	/**
	 * Constructor Principal
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	PuntoCartesiano(int x, int y, int z) {
		this.setX(x);
		this.setY(y);
		this.setZ(z);
	}

	/**
	 * Constructor copia
	 * 
	 * @param PuntoCartesiano
	 */
	PuntoCartesiano(PuntoCartesiano punto) {
		this(punto.x, punto.y, punto.z);
	}

	/**
	 * Constructor por defecto.
	 */
	PuntoCartesiano() {
		this(0, 0, 0);
	}

	/**
	 * Getter del atributo x.
	 * 
	 * @return int
	 */
	int getX() {
		return x;
	}

	/**
	 * Setter del atributo x
	 * 
	 * @param x
	 */
	void setX(int x) {
		this.x = x;
	}

	/**
	 * Getter del atributo y.
	 * 
	 * @return int
	 */
	int getY() {
		return y;
	}

	/**
	 * Setter del atributo y
	 * 
	 * @param y
	 */
	void setY(int y) {
		this.y = y;
	}

	/**
	 * Getter del atributo z.
	 * 
	 * @return int
	 */
	int getZ() {
		return z;
	}

	/**
	 * Setter del atributo z
	 * 
	 * @param z
	 */
	void setZ(int z) {
		this.z = z;
	}

}
