/** 
 * Práctica Programación. - Libro.java
 * 
 * Identifica los datos que consideras adecuados para representar el estado de los siguientes objetos teniendo en cuenta el contexto en el que se vayan a utilizar:
 * 
 * Un libro (en una biblioteca)
 * 
 * Crea las correspondientes clases en Java.
 * Define los constructores típicos de cada una de las clases. Implementa los correspondientes métodos para el acceso y la modificación del estado de los objetos (los métodos get y set).
 * Deben seguirse los principios y estilo del código limpio.
 * 
 * @version: 1.0 - 2019/04/04
 * @author: mfp
 */

package e2_Clases;

import java.util.GregorianCalendar;

class Libro {

	// Atributos
	String titulo;
	String autor;
	GregorianCalendar fechaLanzamiento;
	int edicion;
	Localizacion lugar;

	/**
	 * Constructor principal
	 */
	Libro(String titulo, String autor, GregorianCalendar fechaLanzamiento, int edicion, Localizacion lugar) {
		this.setTitulo(new String(titulo));
		this.setAutor(new String(autor));
		this.setFechaLanzamiento(
				new GregorianCalendar(fechaLanzamiento.YEAR, fechaLanzamiento.MONTH, fechaLanzamiento.DAY_OF_MONTH));
		this.setEdicion(Integer.valueOf(edicion));
		this.setPosicion(new Localizacion(lugar));
	}

	/**
	 * Constructor copia
	 */
	Libro(Libro datos) {
		this(datos.titulo, datos.autor, datos.fechaLanzamiento, datos.edicion, datos.lugar);
	}

	/**
	 * Constructor por defecto
	 */
	Libro() {
		this("Diccionario", "Real Academia de la Lengua", new GregorianCalendar(2000, 0, 1), 1, new Localizacion());
	}

	/**
	 * Getter del atributo titulo
	 * 
	 * @return String
	 */
	String getTitulo() {
		return titulo;
	}

	/**
	 * Setter del atributo titulo
	 * 
	 * @param titulo
	 */
	void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * Getter del atributo autor
	 * 
	 * @return String
	 */
	String getAutor() {
		return autor;
	}

	/**
	 * Setter del atributo autor
	 * 
	 * @param autor
	 */
	void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * Getter del atributo fechaLanzamiento
	 * 
	 * @return String
	 */
	GregorianCalendar getFechaLanzamiento() {
		return fechaLanzamiento;
	}

	/**
	 * Setter del atributo fechaLanzamiento
	 * 
	 * @param fechaLanzamiento
	 */
	void setFechaLanzamiento(GregorianCalendar fechaLanzamiento) {
		this.fechaLanzamiento = fechaLanzamiento;
	}

	/**
	 * Getter del atributo edicion
	 * 
	 * @return String
	 */
	int getEdicion() {
		return edicion;
	}

	/**
	 * Setter del atributo edicion
	 * 
	 * @param edicion
	 */
	void setEdicion(int edicion) {
		this.edicion = edicion;
	}

	/**
	 * Getter del atributo lugar
	 * 
	 * @return String
	 */
	Localizacion getPosicion() {
		return lugar;
	}

	/**
	 * Setter del atributo lugar
	 * 
	 * @param lugar
	 */
	void setPosicion(Localizacion posicion) {
		this.lugar = posicion;
	}

}

/**
 * Clase auxiliar para almacenar la localización de un libro dentro de la
 * biblioteca.
 */
class Localizacion {
	String zona;
	String estanteria;
	String balda;

	/**
	 * Constructor principal
	 */
	Localizacion(String zona, String estanteria, String balda) {
		this.setZona(new String(zona));
		this.setEstanteria(new String(estanteria));
		this.setBalda(new String(balda));
	}

	/**
	 * Constructor copia
	 */
	Localizacion(Localizacion lugar) {
		this(lugar.zona, lugar.estanteria, lugar.balda);
	}

	/**
	 * Constructor por defecto.
	 */
	Localizacion() {
		this("Almacén", "Nuevas entradas", "La de abajo");
	}

	/**
	 * Getter del atributo zona.
	 * 
	 * @return String
	 */
	private String getZona() {
		return zona;
	}

	/**
	 * Setter del atributo zona.
	 * 
	 * @return zona
	 */
	private void setZona(String zona) {
		this.zona = zona;
	}

	/**
	 * Getter del atributo estanteria.
	 * 
	 * @return String
	 */
	private String getEstanteria() {
		return estanteria;
	}

	/**
	 * Setter del atributo estanteria.
	 * 
	 * @return estanteria
	 */
	private void setEstanteria(String estanteria) {
		this.estanteria = estanteria;
	}

	/**
	 * Getter del atributo balda.
	 * 
	 * @return String
	 */
	private String getBalda() {
		return balda;
	}

	/**
	 * Setter del atributo balda.
	 * 
	 * @return balda
	 */
	private void setBalda(String balda) {
		this.balda = balda;
	}

}