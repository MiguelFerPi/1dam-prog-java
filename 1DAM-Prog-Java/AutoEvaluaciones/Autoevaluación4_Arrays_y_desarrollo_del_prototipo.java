///**
// * Autoevaluación4_Arrays_y_desarrollo_del_prototipo.java
// * Programa que almacena usuarios en un array omitiendo
// * posiciones según un patron dado.
// * mfp - 2018.12.18
// */
//
//class Usuario {
//
//	// Atributos
//	public String nif;
//	public String nombre;
//	public String apellidos;
//	public String correo;
//	public String domicilio;
//	public String fechaNacimiento;
//	public String fechaAlta;
//	public String claveAcceso;
//	public String rol;
//
//} // class
//
//public class Autoevaluación4_Arrays_y_desarrollo_del_prototipo {
//
//	final static int MAX_USUARIOS = 45;
//
//	// Almacén de datos resuelto con arrays
//	public static Usuario[] datosUsuarios = new Usuario[MAX_USUARIOS];
//
//	public static void main(String[] args) {
//
//		int[] patron = { 1, 0, 1, 0, 0, 1 };
//		cargarDatosUsuariosPatron(patron);
//
//		mostrarDatosUsuarios();
//
//	}
//
//	private static void cargarDatosUsuariosPatron(int[] patron) {
//
//		for (int i = 0; i < datosUsuarios.length; i++) {
//
//			// Permite que el patrón se repita tantas veces como sea necesario.
//			int posicionPatron = i % patron.length;
//			
//			if (patron[posicionPatron] == 1) {
//
//				datosUsuarios[i] = new Usuario();				
//				datosUsuarios[i].nif =				String.format("%08d%s", i, "T");
//				datosUsuarios[i].nombre =			"Manolo";
//				datosUsuarios[i].apellidos =		"Lopez Perez";
//				datosUsuarios[i].correo =			"manololp@mail.es";
//				datosUsuarios[i].domicilio =		"Calle Falsa, 123";
//				datosUsuarios[i].fechaNacimiento =	"2000/01/01";
//				datosUsuarios[i].fechaAlta =		"2017/05/09";
//				datosUsuarios[i].claveAcceso =		String.format("%s%04d", "PacoPelos#", i);
//				datosUsuarios[i].rol =				"USUARIO";
//
//			}
//		}
//	}
//
//	private static void mostrarDatosUsuarios() {
//		for (Usuario usuario : datosUsuarios) {
//			if (usuario != null) {
//				System.out.println(String.format(
//						  "%-22s %s\n"
//						+ "%-22s %s\n"
//						+ "%-22s %s\n"
//						+ "%-22s %s\n"
//						+ "%-22s %s\n"
//						+ "%-22s %s\n"
//						+ "%-22s %s\n"
//						+ "%-22s %s\n"
//						+ "%-22s %s\n",
//						"NIF:", 				usuario.nif,
//						"Nombre:", 				usuario.nombre,
//						"Apellidos:", 			usuario.apellidos,
//						"Correo:", 				usuario.correo ,
//						"Domicilio:", 			usuario.domicilio,
//						"Fecha de Nacimiento:", usuario.fechaNacimiento,
//						"Fecha de Alta:", 		usuario.fechaAlta,
//						"Clave de Acceso:", 	usuario.claveAcceso,
//						"Rol:", 				usuario.rol
//				));
//			}
//
//		}
//	}
//
//}
