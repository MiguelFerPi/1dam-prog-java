/** 
 * Práctica Programación. - Proveedor.java
 * 
 * Implementación de la clase Proveedor que hereda de Persona.
 * 
 * @version: 1.0 - 2019/05/11
 * @author: mfp
 */
package Autoevaluación8_Abstracción_y_herencia;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Proveedor extends Persona {

	String idProveedor;
	Calendar fechaAlta;

	/**
	 * Constructor convencional
	 * 
	 * @param nombre
	 * @param direccion
	 * @param telefono
	 * @param correo
	 */
	public Proveedor(String nombre, String direccion, String telefono, String correo, Calendar fechaAlta) {
		super(nombre, direccion, telefono, correo);
		setFechaAlta(fechaAlta);
		setIdProveedor();
	}

	/**
	 * Constructor por defecto.
	 */
	public Proveedor() {
		this("Pablito Pérez López", "Calle Falsa, 123", "+000 (000) 000 000", "mail@mail.es", new GregorianCalendar());
	}

	/**
	 * Constructor copia.
	 */
	public Proveedor(Proveedor proveedor) {
		this(proveedor.nombre, proveedor.direccion, proveedor.telefono, proveedor.correo, proveedor.fechaAlta);
	}

	/**
	 * Devuelve la ID del Proveedor.
	 * 
	 * @return idProveedor
	 */
	public String getIdProveedor() {
		return idProveedor;
	}

	/**
	 * Establece la ID del Proveedor según criterios del ejercicio.
	 * 
	 * Debe estar formado por las tres primeras letras del nombre en mayúsculas y
	 * los tres últimos dígitos del número de teléfono. Se debe utilizar un método
	 * interno para generarlo.
	 */
	private void setIdProveedor() {
		this.idProveedor = this.nombre.substring(0, 3).toUpperCase() + ""
				+ this.telefono.substring(this.telefono.length() - 3, this.telefono.length());
	}

	/**
	 * Devuelve la fecha de alta.
	 * 
	 * @return fechaAlta
	 */
	public Calendar getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * Establece la fecha de alta.
	 * 
	 * @param fechaAlta
	 * @throws Exception
	 */
	public void setFechaAlta(Calendar fechaAlta) {
		assert fechaAlta != null;
		if (fechaAltaValida(fechaAlta)) {
			this.fechaAlta = fechaAlta;
		} else {
			this.fechaAlta = new GregorianCalendar();
		}

	}

	/**
	 * Comprueba si la fecha de alta es válida, falso si es nula o posterior a hoy.
	 * 
	 * @param fechaAlta
	 * @return boolean
	 */
	private boolean fechaAltaValida(Calendar fechaAlta) {
		if (fechaAlta == null) {
			return false;
		}
		if (fechaAlta.after(new GregorianCalendar())) {
			return false;
		}
		return true;
	}

	/**
	 * @return el texto con todos el valor de todos los atributos
	 */
	@Override
	public String toString() {
		return super.toString() + "\n" + idProveedor + "\n" + fechaAlta.get(Calendar.DAY_OF_MONTH) + "/"
				+ (fechaAlta.get(Calendar.MONTH) + 1) + "/" + fechaAlta.get(Calendar.YEAR);
	}

}
