/** 
 * Práctica Programación. - Producto.java
 * 
 * Implementación de la clase Producto.
 * 
 * @version: 1.0 - 2019/05/11
 * @author: mfp
 */
package Autoevaluación8_Abstracción_y_herencia;

public class Producto {

	private String nombre;
	private double precioCoste;
	private TipoProducto tipo;
	private Proveedor suministrador;

	public enum TipoProducto {
		NACIONAL, IMPORTADO
	};

	/**
	 * Constructor convencional
	 * 
	 * @param nombre
	 * @param precioCoste
	 * @param suministrador
	 * @param tipo
	 */
	public Producto(String nombre, double precioCoste, Proveedor suministrador, TipoProducto tipo) {
		setNombre(nombre);
		setPrecioCoste(precioCoste);
		setSuministrador(suministrador);
		setTipo(tipo);
	}

	/**
	 * Constructor por defecto.
	 */
	public Producto() {
		this("Producto", 1.0, new Proveedor(), TipoProducto.NACIONAL);
	}

	/**
	 * Constructor copia.
	 */
	public Producto(Producto producto) {
		this(producto.nombre, producto.precioCoste, producto.suministrador, producto.tipo);
	}

	/**
	 * Devuelve el nombre del producto.
	 * 
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el nombre del producto.
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		assert nombre != null;
		if (nombreValido(nombre)) {
			this.nombre = nombre;
		}
	}

	/**
	 * Comprueba la valides del nombre, no puede ser vacío ni en blanco.
	 * 
	 * @param nombre
	 * @return true si cumple
	 */
	private boolean nombreValido(String nombre) {
		if (nombre.equals("") || nombre.equals(" ")) {
			return false;
		}
		return true;
	}

	/**
	 * Devuelve el precio del producto.
	 * 
	 * @return precioCoste
	 */
	public double getPrecioCoste() {
		return precioCoste;
	}

	/**
	 * Establece el precio del producto.
	 * 
	 * Establecerá a 1.0 si el valor introducido es inválido.
	 * 
	 * @param precioCoste
	 */
	public void setPrecioCoste(double precioCoste) {
		if (precioValido(precioCoste)) {
			this.precioCoste = precioCoste;
		} else {
			this.precioCoste = 1.0;
		}
	}

	/**
	 * Comprueba la validez del precio. No debe ser negativo.
	 * 
	 * @param precioCoste
	 * @return
	 */
	private boolean precioValido(double precioCoste) {
		if (precioCoste <= 0) {
			return false;
		}
		return true;
	}

	/**
	 * Devuelve el tipo de producto.
	 * 
	 * @return tipoProducto
	 */
	public TipoProducto getTipo() {
		return tipo;
	}

	/**
	 * Establece el tipo de producto.
	 * 
	 * @param tipo
	 */
	public void setTipo(TipoProducto tipo) {
		assert tipo != null;
		this.tipo = tipo;
	}

	/**
	 * Devuelve el suministrador.
	 * 
	 * @return suministrador
	 */
	public Proveedor getSuministrador() {
		return suministrador;
	}

	/**
	 * Establece el suministrador.
	 * 
	 * @param suministrador
	 */
	public void setSuministrador(Proveedor suministrador) {
		assert suministrador != null;
		this.suministrador = suministrador;
	}

}
