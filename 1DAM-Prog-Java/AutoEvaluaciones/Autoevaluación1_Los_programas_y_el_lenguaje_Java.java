/**
 * Autoevaluación1_Los_programas_y_el_lenguaje_Java.java                                                             
 * Programa simple que procesa datos introducidos por el usuario y muestra un resultado basado en ellos.   
 * mfp - 2018.10.05                                                         
 */

import java.util.Scanner;

public class Autoevaluación1_Los_programas_y_el_lenguaje_Java
{
    public static void main(String argumentos[]) {
        
        // Creamos una interfaz "Scanner" para poder aceptar datos
        // introducidos por el usuario.
        Scanner teclado = new Scanner(System.in);

        int num;               // Se declara la variable "num"
       
        // Muestra un mensaje de bienvenida.
        num = 1;
        System.out.println("Hola, Soy un modesto ");
        System.out.print("programa de ordenador...\n");
        System.out.println("Mi número de orden es el " + num + " por ser el primero.");

        // Se pide al usuario datos para procesarlos.
        System.out.println("Escribe dos números...\n");
        int dato1 = teclado.nextInt();
        int dato2 = teclado.nextInt();

        // Se muestran los datos introducidos anteriormente.
        System.out.println("Dato1: " + dato1);
        System.out.println("Dato2: " + dato2);

        // Se declara la variable mensaje y se actualiza en función de lo procesado por el código.
        String mensaje = "";

        if (dato1 == dato2) {
            mensaje = "Ninguno de los dos es mayor... ";
        }

        if (dato1 > dato2) {
            mensaje = "El mayor es: " + dato1;
        }

        if (dato2 > dato1) {
            mensaje = "El mayor es: " + dato2;
        }

        System.out.println(mensaje);
    }

}