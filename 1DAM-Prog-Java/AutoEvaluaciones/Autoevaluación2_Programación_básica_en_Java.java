/**
 * Autoevaluación2_Programación_básica_en_Java.java
 * Programa que usa muestra el menor y el mayor de todos los números enteros positivos introducidos
 * mfp - 2018.10.27
 */

import java.util.Scanner;

public class Autoevaluación2_Programación_básica_en_Java {
	public static void main(String[] args) {

		// Iniciamos Scanner.
		Scanner teclado = new Scanner(System.in);

		// Declaramos las variables LONG con sus valores iniciales.
		long elMayor = 0;
		long elMenor = Long.MAX_VALUE;
		// Declaramos una variable LONG para almacenar temporalmente el último número introducido.
		long valorEntrada;
		// Declaramos una variable INT para controlar cuántos números han sido introducidos.
		int contadorDatosValidos = 0;

		// Indicamos al usuario qué vamos a pedir.
		System.out.println("Introduzca números enteros positivos y al final le mostraré cuál es el mayor y el menor de todos.");
		System.out.println("Introduzca un 0 si desea terminar, se necesitan introducir al menos 2 números.\n");

		// Iniciamos el bucle con el que pediremos los números a comparar.
		while (true) {
			System.out.print((contadorDatosValidos+1)+"º número: ");
			valorEntrada = Long.valueOf(teclado.nextLine()); // Almacenamos temporalmente el número introducido.
			teclado = new Scanner(System.in); // Limpiamos el búfer de Scanner.

			if (valorEntrada == 0) {
				if (contadorDatosValidos < 2) {
					System.out.println("Debe introducir al menos 2 números.");
					System.out.print("Si aun así desea terminar el programa, pulse \"T\". ");
					if (teclado.nextLine().toLowerCase().equals("t")){
						break;
					} else {
						teclado = new Scanner(System.in); // Limpiamos el búfer de Scanner.
						continue;
					}
				} else {
					System.out.print("Ha introducido 0, para confirmar la salida del programa, pulse \"T\". ");
					if (teclado.nextLine().toLowerCase().equals("t")){
						break;
					} else {
						teclado = new Scanner(System.in); // Limpiamos el búfer de Scanner.
						continue;
					}
				}
			}

			if (elMayor < valorEntrada){ // Si el número introducido es mayor que el almacenado, se reemplaza con el nuevo.
				elMayor = valorEntrada; 
			}
			if (elMenor > valorEntrada){ // Si el número introducido es menor que el almacenado, se reemplaza con el nuevo.
				elMenor = valorEntrada; 
			}
			
			contadorDatosValidos++;
		}
		
		teclado = new Scanner(System.in); // Limpiamos el búfer de Scanner.
		System.out.println("");
		if (contadorDatosValidos < 2) {
			System.out.println("Salida del programa sin resultados al no introducir la cantidad mínima de números.");
		} else {
			System.out.println("Ha introducido un total de "+ contadorDatosValidos +" números.");
			System.out.println("El mayor ha sido: "+ elMayor);
			System.out.println("El menor ha sido: "+ elMenor);
		}
	}
}
