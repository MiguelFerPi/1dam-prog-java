/** 
 * Autoevaluación3_Programación_estructurada_en_Java.java
 * Programa que muestra un diagrama de barras horizontales que representan valores previamente introducidos.
 * mfp - 2018.11.13
 */

public class Autoevaluación3_Programación_estructurada_en_Java {

	public static void main(String[] args) {

		generarBarraHorizontal(9);
		generarBarraHorizontal(-2);
		generarBarraHorizontal(4);
		generarBarraHorizontal(-6);
		generarBarraHorizontal(-10);
		generarBarraHorizontal(0);
		generarBarraHorizontal(10);
		generarBarraHorizontal(-11);
		generarBarraHorizontal(11);
		
	}

	// Declaración de constantes globales
	static final String CARACTER_BASE = "*";
	static final int LIMITE_SUPERIOR = 10;
	static final int LIMITE_INFERIOR = -10;
	
	// Creará cada línea del diagrama.
	static void generarBarraHorizontal(int valor) {

		// Si el valor es positivo, se le añade un espacio para que se alinee mejor con los negativos.
		if (valor >= 0) {
			System.out.print(" " + valor);
		} else {
			System.out.print(valor);
		}

		// Se comprueba si el valor está dentro del rango permitido.
		if (LIMITE_INFERIOR <= valor & valor <= LIMITE_SUPERIOR) {

			System.out.print("\t" + generarBloqueCaracteres(valor,CARACTER_BASE) + "\n");

		} else {
			System.out.print("\tFUERA DE RANGO\n");
		}
		
		
	}
	
	// Creará la parte del diagrama de cada línea.
	static String generarBloqueCaracteres(int valor,String caracter) {

		String bloque = "";
		
		if (valor < 0) {
			// Si es negativo se dibuja hacia la izquierda.
			bloque = repetir(" ", Math.abs(LIMITE_INFERIOR) - Math.abs(valor))
					+ repetir(caracter, Math.abs(valor)) + "|";
		} else {
			//Si es positivo se dibuja hacia la derecha.
			bloque = repetir(" ", Math.abs(LIMITE_INFERIOR))
					+ "|" + repetir(caracter, Math.abs(valor));
		}
		
		return bloque;
	}
	
	// Método que creará los carácteres necesarios basandose en repetición del caracter.
	static String repetir(String texto, int veces) {

		String textoResultante = "";
		
		for (int n = 1; n <= veces; n++) {
			textoResultante = textoResultante + texto;  
		}
		
		return textoResultante;
	}

}

