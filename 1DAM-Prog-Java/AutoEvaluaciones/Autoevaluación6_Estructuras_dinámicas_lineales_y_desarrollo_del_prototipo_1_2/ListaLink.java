package Autoevaluación6_Estructuras_dinámicas_lineales_y_desarrollo_del_prototipo_1_2;
/**
 * Representa la implementación parcial de una lista enlazada simple en la que
 * se tiene acceso directo al primer elemento y también al último.
 */
public class ListaLink {

    /**
     * La clase Nodo está anidada y representa la estructura de un elemento de
     * la lista enlazada simple.
     */
    class Nodo {

        //Atributos
        Object dato;
        Nodo siguiente;

        /**
         * Constructor que inicializa atributos al valor por defecto.
         */
        public Nodo() {
            dato = null;
            siguiente = null;
        }

    } //class Nodo


    //Atributos ListaLink
    private Nodo primero;
    private Nodo ultimo;
    private int numElementos;

    /**
     * Constructor que inicializa los atributos al valor por defecto
     */
    public ListaLink() {
        primero = null;
        ultimo = null;
        numElementos = 0;
    }
    
	/**
	 * Añade un elemento en la posicion indicada
	 * 
	 * @param indice
	 * @param dato
	 */
	public void add(int indice, Object dato) {		
		Nodo nuevo = new Nodo();
		nuevo.dato = dato;
		Nodo anterior = null;

		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("Índice incorrecto: " + indice);
		}

		nuevo.siguiente = obtenerNodo(indice);

		if (indice == 0) {
			if (primero.siguiente != null) {
				Nodo segundo = obtenerNodo(0);
				primero = nuevo;
				primero.siguiente = segundo;
			} else {
				primero = nuevo;
			}
		} else {
			anterior = obtenerNodo(indice - 1);
			anterior.siguiente = nuevo;
		}
		numElementos++; // Actualiza el número de elementos.
	}
	
	/**
	 * Añade un elemento al final de la lista.
	 * 
	 * @param elem - el elemento a añadir. Admite que el elemento a añadir sea null.
	 */
	public void add(Object dato) {
		// variables auxiliares
		Nodo nuevo = new Nodo();
		nuevo.dato = dato;
		Nodo ultimo = null;
		if (numElementos == 0) {
			// Si la lista está vacía enlaza el nuevo nodo el primero.
			primero = nuevo;
		} else {
			// Obtiene el último nodo y enlaza el nuevo.
			ultimo = obtenerNodo(numElementos - 1);
			ultimo.siguiente = nuevo;
		}
		numElementos++; // Actualiza el número de elementos.
	}
	
	/**
	 * Obtiene el nodo correspondiente al índice. Recorre secuencialmente la cadena
	 * de enlaces.
	 * 
	 * @param indice - posición del nodo a obtener.
	 * @return - el nodo que ocupa la posición indicada por el índice.
	 * @exception IndexOutOfBoundsException - índice no está entre 0 y
	 *                                      numElementos-1
	 */
	private Nodo obtenerNodo(int indice) {
		// Lanza excepción si el índice no es válido.
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("Índice incorrecto: " + indice);
		}
		// Recorre la lista hasta llegar al nodo de posición buscada.
		Nodo actual = primero;
		for (int i = 0; i < indice; i++)
			actual = actual.siguiente;
		return actual;
	}
	
	/**
	 * @param indice – obtiene un elemento por su índice.
	 * @return elemento contenido en el nodo indicado por el índice.
	 * @exception IndexOutOfBoundsException - índice no está entre 0 y
	 *                                      numElementos-1.
	 */
	public Object get(int indice) {
		// lanza excepción si el índice no es válido
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("índice incorrecto: " + indice);
		}
		Nodo aux = obtenerNodo(indice);
		return aux.dato;
	}


	public static void main(String[] args) {
		ListaLink listaCompra = new ListaLink();
		listaCompra.add("Leche");
		listaCompra.add("Miel");
		listaCompra.add("Aceitunas");
		listaCompra.add("Cerveza");
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.numElementos; i++) {
			System.out.println(listaCompra.get(i));
		}
		System.out.println("====================");

		// Se nos olvidó el café y es más importante que todo menos la leche.
		listaCompra.add(1, "Café");
		System.out.println("Lista de la compra:");
		for (int i = 0; i < listaCompra.numElementos; i++) {
			System.out.println(listaCompra.get(i));
		}

	}

} //class listaLink