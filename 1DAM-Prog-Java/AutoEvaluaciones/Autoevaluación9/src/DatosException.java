/** 
 * Autoevaluación.9 - DatosException.java
 * 
 * Clase para lanzar excepciones del proyecto.
 * 
 * @version: 1.0 - 2019/06/08
 * @author: mfp
 */
package Autoevaluación9.src;

public class DatosException extends Exception {
	
	public DatosException(String mensaje) {
		super(mensaje);
	}

	public DatosException() {
		super();
	}
}

