/** 
 * Autoevaluación.9 - Empresa.java
 * 
 * Representa el concepto de empresa en una aplicación de gestión.
 * 
 * @version: 1.0 - 2019/06/08
 * @author: mfp
 */
package Autoevaluación9.src;

import java.io.Serializable;

public class Empresa implements Serializable {

	private static final long serialVersionUID = 1L;

	// ENUM de TIPOS de empresa
	public enum TIPOS {
		INDUSTRIAL, SERVICIOS, OTROS
	}

	// Atributos
	private String id;
	private String nombreEmpresa;
	private String cif;
	private String telefono;
	private TIPOS tipoEmpresa = TIPOS.SERVICIOS;

	/**
	 * Constructor convencional
	 * 
	 * Lanza DatosException si los datos introducidos no son válidos.
	 * 
	 * @throws DatosException
	 */
	public Empresa(String id, String nombreEmpresa, String cif, String telefono, TIPOS tipoEmpresa)
			throws DatosException {
		this.setId(id);
		this.setNombreEmpresa(nombreEmpresa);
		this.setCif(cif);
		this.setTelefono(telefono);
		this.setTipoEmpresa(tipoEmpresa);
	}

	/**
	 * Constructor por defecto
	 * 
	 * Lanza DatosException si los datos introducidos no son válidos.
	 * 
	 * @throws DatosException
	 */
	public Empresa() throws DatosException {
		this("EMP1", "Empresa Prueba 1", "00000000Z", "600000000", TIPOS.SERVICIOS);
	}

	/**
	 * Constructor copia
	 * 
	 * Lanza DatosException si los datos introducidos no son válidos.
	 * 
	 * @throws DatosException
	 */
	public Empresa(Empresa empresa) throws DatosException {
		if (empresa == null || empresa.getClass() != getClass()) {
			throw new DatosException("Objeto Empresa a copiar es inválido");
		} else {
			this.setId(empresa.id);
			this.setNombreEmpresa(empresa.nombreEmpresa);
			this.setCif(empresa.cif);
			this.setTelefono(empresa.telefono);
			this.setTipoEmpresa(empresa.tipoEmpresa);
		}
	}

	/**
	 * Devuelve la ID de la Empresa
	 * 
	 * @return id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Establece el ID de la Empresa.
	 * 
	 * Lanza DatosException si el ID introducido no es válido.
	 * 
	 * @param id
	 * @throws DatosException
	 */
	public void setId(String id) throws DatosException {
		assert id != null;
		if (validar(id)) {
			this.id = id;
		} else {
			throw new DatosException("El ID introducido no es válido.");
		}
	}

	/**
	 * Devuelve el Nombre de la Empresa
	 * 
	 * @return nombreEmpresa
	 */
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	/**
	 * Establece el Nombre de la Empresa
	 * 
	 * Lanza DatosException si el Nombre introducido no es válido.
	 * 
	 * @param nombreEmpresa
	 * @throws DatosException
	 */
	public void setNombreEmpresa(String nombreEmpresa) throws DatosException {
		assert nombreEmpresa != null;
		if (validar(nombreEmpresa)) {
			this.nombreEmpresa = nombreEmpresa;
		} else {
			throw new DatosException("El Nombre de Empresa introducido no es válido.");
		}
	}

	/**
	 * Devuelve el CIF de la Empresa
	 * 
	 * @return cif
	 */
	public String getCif() {
		return cif;
	}

	/**
	 * Establece el CIF de la Empresa
	 * 
	 * Lanza DatosException si el CIF introducido no es válido.
	 * 
	 * @param cif
	 * @throws DatosException
	 */
	public void setCif(String cif) throws DatosException {
		assert cif != null;
		if (validar(cif)) {
			this.cif = cif;
		} else {
			throw new DatosException("El CIF introducido no es válido.");
		}
	}

	/**
	 * Devuelve el Teléfono de la Empresa
	 * 
	 * @return telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * Establece el Teléfono de la Empresa
	 * 
	 * Lanza DatosException si el Teléfono introducido no es válido.
	 * 
	 * @param telefono
	 * @throws DatosException
	 */
	public void setTelefono(String telefono) throws DatosException {
		assert telefono != null;
		if (validar(telefono)) {
			this.telefono = telefono;
		} else {
			throw new DatosException("El Teléfono introducido no es válido.");
		}
	}

	/**
	 * Comprueba que el dato introducido no sea nulo o vacío
	 * 
	 * @param dato
	 * @return true si es válido
	 */
	private boolean validar(String dato) {
		if (dato == null || dato.matches("^\\s+$") || dato.isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * Devuelve el Tipo de Empresa
	 * 
	 * @return tipoEmpresa
	 */
	public TIPOS getTipoEmpresa() {
		return tipoEmpresa;
	}

	/**
	 * Establece el Tipo de Empresa
	 * 
	 * @param tipoEmpresa
	 */
	public void setTipoEmpresa(TIPOS tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}

	/*
	 * Devuelve los datos de la Empresa como String.
	 */
	@Override
	public String toString() {
		return String.format("Empresa ID: %s\n" + "Nombre: %s\n" + "CIF: %s\n" + "Teléfono: %s\n" + "Tipo: %s\n",
				this.id, this.nombreEmpresa, this.cif, this.telefono, this.tipoEmpresa);
	}
}
