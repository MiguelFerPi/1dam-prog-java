/** 
 * Autoevaluación.9 - Datos.java
 * 
 * La clase Datos representa el almacén de datos del programa.
 * Sigue el patrón Singleton  y contiene la base de datos orientada a objetos del programa.
 * Implementos los métodos indicados en la interface  OperacionesDatos.
 * 
 * @version: 1.0 - 2019/06/08
 * @author: mfp
 */
package Autoevaluación9.src;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Query;

public class Datos implements OperacionesDatos {

	// Singleton
	private static Datos datos;
	private static ObjectContainer db;

	public Datos Datos() {
		if (datos == null) {
			initConexion();
		}
		return datos;
	}

	/**
	 * Configura la conexion a la base de datos.
	 */
	private void initConexion() {
		final String PATH = "./empresas.db4o";
		EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
		config.common().objectClass(Calendar.class).callConstructor(true);
		db = Db4oEmbedded.openFile(config, PATH);
	}

	/**
	 * Cierra la conexión a la base de datos.
	 */
	public static void cerrarConexiones() {
		if (db != null) {
			db.close();
		}
	}

	// Métodos de manejo de datos

	/**
	 * Da de alta un nuevo objeto en la base de datos.
	 * 
	 * @param obj - Objeto que contiene los datos de Empresa
	 */
	@Override
	public void alta(Object obj) throws DatosException {
		ObjectSet<Empresa> result = db.queryByExample(obj);
		if (result.size() == 0) {
			db.store(obj);
		} else {
			throw new DatosException("El Objeto Empresa ya existe en la base de datos");
		}
	}

	/**
	 * Importa objetos serializados de un fichero dado.
	 * 
	 * @param fDatos Fichero que contiene objetos Empresa a importar
	 */
	@Override
	public void importar(File fDatos) throws DatosException {
		if (fDatos.exists()) {
			try {
				FileInputStream fisEmpresas = new FileInputStream(fDatos);
				ObjectInputStream oisEmpresas = new ObjectInputStream(fisEmpresas);
				ArrayList<Serializable> datosEmpresa = (ArrayList<Serializable>) oisEmpresas.readObject();
				for (Object obj : datosEmpresa) {
					alta(obj);
				}
				oisEmpresas.close();
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Método para vaciar la Base de Datos de objetos Empresa.
	 */
	public void vaciarBDEmpresa() {
		Query query = db.query();
		query.constrain(Empresa.class);
		ObjectSet<Empresa> result = query.execute();
		for (Empresa empresa : result) {
			db.delete(empresa);
		}
	}

}
