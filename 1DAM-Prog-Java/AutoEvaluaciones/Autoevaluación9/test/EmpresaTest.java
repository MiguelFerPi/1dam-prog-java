/** 
 * Autoevaluación.9 - EmpresaTest.java
 * 
 * (DESCRIPCION EJERCICIO)
 * 
 * @version: 1.0 - 2019/06/08
 * @author: mfp
 */
package Autoevaluación9.test;

import java.io.File;
import org.junit.*;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import Autoevaluación9.src.Datos;
import Autoevaluación9.src.DatosException;
import Autoevaluación9.src.Empresa;
import Autoevaluación9.src.Empresa.TIPOS;

public class EmpresaTest implements PruebasBasicas {

	private static Datos datos;
	private static Empresa empresa1;
	private Empresa empresa2;

	/**
	 * Método que se ejecuta antes de cada @Test para preparar datos de prueba.
	 */
	@Before
	@Override
	public void crearObjetosPrueba() {
		try {
			empresa1 = new Empresa("PAQSA", "Paquito SA", "00000001H", "600000001", TIPOS.SERVICIOS);
		} catch (DatosException e) {
			fail("No debería llegar a aquí");
		}
	}

	/**
	 * Método que se ejecuta después de cada @Test para limpiar datos de prueba.
	 */
	@After
	@Override
	public void borrarObjetosPrueba() {
		empresa1 = null;
		//datos.vaciarBDEmpresa();
		datos.cerrarConexiones();
	}

	/**
	 * Prueba constructor normal con parámetros correctos para los atributos.
	 */
	@Test
	@Override
	public void testConstructorNormal() {
		try {
			empresa2 = new Empresa("ROSL", "Romualdo SL", "00000005H", "600000005", TIPOS.INDUSTRIAL);
		} catch (DatosException e) {
			fail("No debería llegar a aquí");
		}

		assertEquals("ROSL", empresa2.getId());
		assertEquals("Romualdo SL", empresa2.getNombreEmpresa());
		assertEquals("00000005H", empresa2.getCif());
		assertEquals("600000005", empresa2.getTelefono());
		assertEquals(TIPOS.INDUSTRIAL, empresa2.getTipoEmpresa());
	}

	/**
	 * Prueba constructor normal con datos incorrcorrectos para los atributos.
	 */
	@Test
	@Override
	public void testConstructorNormalIncorrectos() {
		try {
			empresa2 = new Empresa("", "Romualdo SL", "00000005H", "600000005", TIPOS.INDUSTRIAL);
		} catch (DatosException e) {
		}
		try {
			empresa2 = new Empresa("ROSL", "", "00000005H", "600000005", TIPOS.INDUSTRIAL);
		} catch (DatosException e) {
		}
		try {
			empresa2 = new Empresa("ROSL", "Romualdo SL", "", "600000005", TIPOS.INDUSTRIAL);
		} catch (DatosException e) {
		}
		try {
			empresa2 = new Empresa("ROSL", "Romualdo SL", "00000005H", "", TIPOS.INDUSTRIAL);
		} catch (DatosException e) {
		}

		assertNull(empresa2);
	}

	/**
	 * Prueba constructor por defecto.
	 */
	@Test
	@Override
	public void testConstructorDefecto() {
		try {
			empresa2 = new Empresa();
		} catch (DatosException e) {
			fail("Esto no debería pasar");
		}

		assertEquals("EMP1", empresa2.getId());
		assertEquals("Empresa Prueba 1", empresa2.getNombreEmpresa());
		assertEquals("00000000Z", empresa2.getCif());
		assertEquals("600000000", empresa2.getTelefono());
		assertEquals(TIPOS.SERVICIOS, empresa2.getTipoEmpresa());
	}

	/**
	 * Prueba constructor copia con objeto correcto.
	 */
	@Test
	@Override
	public void testConstructorCopia() {
		try {
			empresa2 = new Empresa(empresa1);
		} catch (DatosException e) {
			fail("Esto no debería pasar");
		}

		assertEquals(empresa1.getId(), empresa2.getId());
		assertEquals(empresa1.getNombreEmpresa(), empresa2.getNombreEmpresa());
		assertEquals(empresa1.getCif(), empresa2.getCif());
		assertEquals(empresa1.getTelefono(), empresa2.getTelefono());
		assertEquals(empresa1.getTipoEmpresa(), empresa2.getTipoEmpresa());
	}

	/**
	 * Prueba constructor copia con objeto incorrecto.
	 */
	@Test
	@Override
	public void testConstructorCopiaIncorrecto() {
		try {
			empresa2 = new Empresa(null);
			fail("No debería llegar hasta aquí");
		} catch (DatosException e) {
		}
	}

	/**
	 * Prueba de salida con toString()
	 */
	@Test
	@Override
	public void testToString() {
		assertEquals("Empresa ID: PAQSA\n" + "Nombre: Paquito SA\n" + "CIF: 00000001H\n" + "Teléfono: 600000001\n"
				+ "Tipo: SERVICIOS\n", empresa1.toString());
	}

	/**
	 * Pruebas para alta de un objeto en la base de datos.
	 */
	@Test
	@Override
	public void testAltaDatos() {
		try {
			datos.alta(empresa1);
		} catch (DatosException e) {
			fail("No debería pasar");
		}

	}

	/**
	 * Pruebas para la importación de datos desde un fichero.
	 */
	@Test
	@Override
	public void testImportarDatos() {
		try {
			datos.importar(new File("./importar.txt"));
		} catch (DatosException e) {
			fail("No debería pasar");
		}
	}

}
