/**
 * Clase que contiene un array de objetos de la clase Usuario
 * y métodos para las pruebas y manipulación del array.
 * @author ajp
 */
public class Autoevaluación5_Cadenas_de_caracteres_y_desarrollo_del_prototipo_1_1 {

    // Vector de objetos
    public static Usuario[] datosUsuarios;

    /**
     * Método principal que coordina la
     * llamada a los distintos métodos probados.
     * @param args
     */
    public static void main(String[] args) {

        //Prepara texto para la prueba con una cantidad arbitraria de objetos Usuario
        //Cada objeto va separado por: ;
        //Los atributos van separados por: ,

		String texto = "0344556K,pepe0,López Pérez0,C/Luna 27 30132 Murcia,pepe0@gmail.com,"
					 + "1990.11.12,2015.12.3,miau0,usuario normal;";

        //Llamada al método de importación de usuarios

    	importarUsuariosTexto(texto);

    }

    
	/**
	 * Recibe una entrada de texto y añadirá un nuevo usuario
	 * al array "datosUsuarios" en base a dicha entrada.
	 * @param texto
	 * @author mfp
	 */
	private static void importarUsuariosTexto(String texto) {

		String[] entrada = texto.split(";");

		Usuario[] datosUsuariosNuevo = new Usuario[entrada.length + 20];
		
		// Índice del array de datos de entrada.
		for (int i = 0; i < entrada.length; i++) {
			String[] usuario = entrada[i].split(",");
			datosUsuariosNuevo[i] = new Usuario();
			datosUsuariosNuevo[i].nif = usuario[0]; 
			datosUsuariosNuevo[i].nombre = usuario[1]; 
			datosUsuariosNuevo[i].apellidos = usuario[2]; 
			datosUsuariosNuevo[i].domicilio = usuario[3]; 
			datosUsuariosNuevo[i].correo = usuario[4]; 
			datosUsuariosNuevo[i].fechaNacimiento = usuario[5]; 
			datosUsuariosNuevo[i].fechaAlta = usuario[6]; 
			datosUsuariosNuevo[i].claveAcceso = usuario[7]; 
			datosUsuariosNuevo[i].rol = usuario[8]; 
		}
		
		datosUsuarios = datosUsuariosNuevo;
		
	}

} //class



/**
 * Clase que representa el usuario de un sistema.
 * @author ajp
 */
class Usuario {

    //Atributos
    public String nif;
    public String nombre;
    public String apellidos;
    public String domicilio;
    public String correo;
    public String fechaNacimiento;
    public String fechaAlta;
    public String claveAcceso;
    public String rol;

} //class