/**
 * Implementación de la clase Empleado según ejercicio de autoevaluación: https://moodle.iescierva.net/mod/workshop/view.php?id=40405
 * 
 * @source: Empleado.java 
 * @version 1.0 - 2019.03.29
 * @author mfp
 */

package Autoevaluación7_Encapsulación_herencia_básica_y_desarrollo_del_prototipo_2;

public class Empleado {

	private String nombre;
	private int sueldo;
	private int edad;
	private PuestoTrabajo puesto;

	/**
	 * Constructor convencional, usa métodos set...()
	 * 
	 * @param nombre
	 * @param sueldo
	 * @param edad
	 * @param puesto
	 */
	public Empleado(String nombre, int sueldo, int edad, PuestoTrabajo puesto) {
		setNombre(nombre);
		setSueldo(sueldo);
		setEdad(edad);
		setPuesto(puesto);
	}

	/**
	 * Constructor por defecto, redireccional al constructor convencional.
	 */
	public Empleado() {
		this("Paco García Pérez", 11000, 30, new PuestoTrabajo("Oficinista"));
	}

	/**
	 * Constructor copia.
	 * 
	 * @param emp
	 */
	public Empleado(Empleado emp) {
		this(new String(emp.nombre), emp.sueldo, emp.edad, new PuestoTrabajo(emp.puesto));
	}

	/**
	 * Devuelve el nombre del Empleado.
	 * 
	 * @return String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el nombre del Empleado.
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		assert nombre != null;
		if (nombre == null || !validarNombre(nombre)) {
			nombre = new Empleado().nombre;
		}
		this.nombre = nombre;
	}

	/**
	 * Valida si el nombre introducido es correcto, deben ser al menos 3 palabras,
	 * Nombre y 2 apellidos.
	 * 
	 * @param nombre
	 * @return boolean
	 */
	private boolean validarNombre(String nombre) {
		return nombre.matches("^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+ [a-zA-ZñÑáéíóúÁÉÍÓÚ]+ [a-zA-ZñÑáéíóúÁÉÍÓÚ]+$");
	}

	/**
	 * Devuelve el sueldo del Empleado.
	 * 
	 * @return int
	 */
	public int getSueldo() {
		return sueldo;
	}

	/**
	 * Establece el sueldo del Empleado.
	 * 
	 * @param sueldo
	 */
	public void setSueldo(int sueldo) {
		if (sueldo < 10000) {
			sueldo = new Empleado().sueldo;
		}
		this.sueldo = sueldo;
	}

	/**
	 * Devuelve la edad del Empleado.
	 * 
	 * @return int
	 */
	public int getEdad() {
		return edad;
	}

	/**
	 * Establece la edad del Empleado.
	 * 
	 * @param edad
	 */
	public void setEdad(int edad) {
		if (edad < 16 || edad > 65) {
			edad = new Empleado().edad;
		}
		this.edad = edad;
	}

	/**
	 * Devuelve el puesto de trabajo del Empleado.
	 * 
	 * @return PuestoTrabajo
	 */
	public PuestoTrabajo getPuesto() {
		return puesto;
	}

	/**
	 * Establece el puesto de trabajo del Empleado.
	 * 
	 * @param puesto
	 */
	public void setPuesto(PuestoTrabajo puesto) {
		assert puesto != null;
		if (puesto == null) {
			puesto = new Empleado().puesto;
		}
		this.puesto = puesto;
	}

	/**
	 * Devuelve una instancia nueva del objeto Empleado.
	 */
	public Empleado clone() {
		return new Empleado(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + edad;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((puesto == null) ? 0 : puesto.hashCode());
		result = prime * result + sueldo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empleado other = (Empleado) obj;
		if (edad != other.edad)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (puesto == null) {
			if (other.puesto != null)
				return false;
		} else if (!puesto.equals(other.puesto))
			return false;
		if (sueldo != other.sueldo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Empleado\nnombre=" + nombre + "\nsueldo=" + sueldo + "\nedad=" + edad + "\npuesto=" + puesto + "\n";
	}

}

// Clase creada solo para que el atributo puesto no de error.
class PuestoTrabajo {

	private String puesto;

	public PuestoTrabajo(String string) {
		assert string != null;
		puesto = string;
	}

	public PuestoTrabajo(PuestoTrabajo puesto) {
		this.puesto = new String(puesto.getPuesto());
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		assert puesto != null;
		this.puesto = puesto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((puesto == null) ? 0 : puesto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PuestoTrabajo other = (PuestoTrabajo) obj;
		if (puesto == null) {
			if (other.puesto != null)
				return false;
		} else if (!puesto.equals(other.puesto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return puesto;
	}

}