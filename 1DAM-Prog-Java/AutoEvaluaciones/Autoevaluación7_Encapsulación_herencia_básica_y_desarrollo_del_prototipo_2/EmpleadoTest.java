/**
 * Test de métodos de la clase Empleado.java.
 * 
 * @source: EmpleadoTest.java 
 * @version 1.0 - 2019.03.29
 * @author mfp
 */

package Autoevaluación7_Encapsulación_herencia_básica_y_desarrollo_del_prototipo_2;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmpleadoTest {

	public Empleado empleadoPepito;

	/**
	 * Método que se ejecuta antes de cada prueba para reiniciar el Empleado para
	 * los tests.
	 */
	@BeforeEach
	public void nuevoEmpleadoPepito() {
		empleadoPepito = new Empleado("Pepito Cocinillas López", 15000, 20, new PuestoTrabajo("Chef"));
	}

	// Tests de los constructores.

	@Test
	public void nuevoEmpleadoDefecto() {
		Empleado empleadoDefecto = new Empleado();
		assertEquals(new Empleado().getNombre(), empleadoDefecto.getNombre());
		assertEquals(new Empleado().getSueldo(), empleadoDefecto.getSueldo());
		assertEquals(new Empleado().getEdad(), empleadoDefecto.getEdad());
		assertEquals(new Empleado().getPuesto(), empleadoDefecto.getPuesto());
	}

	@Test
	public void testEmpleadoPepito() {
		assertEquals("Pepito Cocinillas López", empleadoPepito.getNombre());
		assertEquals(15000, empleadoPepito.getSueldo());
		assertEquals(20, empleadoPepito.getEdad());
		assertEquals(new PuestoTrabajo("Chef"), empleadoPepito.getPuesto());
	}

	@Test
	public void testCopiaPepito() {
		Empleado copiaPepito = new Empleado(empleadoPepito);
		assertEquals(empleadoPepito.getNombre(), copiaPepito.getNombre());
		assertEquals(empleadoPepito.getSueldo(), copiaPepito.getSueldo());
		assertEquals(empleadoPepito.getEdad(), copiaPepito.getEdad());
		assertEquals(empleadoPepito.getPuesto(), copiaPepito.getPuesto());
		assertNotSame(empleadoPepito.getNombre(), copiaPepito.getNombre());
		assertNotSame(empleadoPepito.getPuesto(), copiaPepito.getPuesto());
	}

	// Test del método clone.

	@Test
	public void testClonPepito() {
		Empleado clonPepito = empleadoPepito.clone();
		assertEquals(empleadoPepito.getNombre(), clonPepito.getNombre());
		assertEquals(empleadoPepito.getSueldo(), clonPepito.getSueldo());
		assertEquals(empleadoPepito.getEdad(), clonPepito.getEdad());
		assertEquals(empleadoPepito.getPuesto(), clonPepito.getPuesto());
		assertNotSame(empleadoPepito.getNombre(), clonPepito.getNombre());
		assertNotSame(empleadoPepito.getPuesto(), clonPepito.getPuesto());
	}

	// Tests con datos válidos.
	@Test
	public void testSetNombre() {
		empleadoPepito.setNombre("Pepe Muñoz Sánchez");
		assertEquals("Pepe Muñoz Sánchez", empleadoPepito.getNombre());
	}

	@Test
	public void testSetSueldo() {
		empleadoPepito.setSueldo(50000);
		assertEquals(50000, empleadoPepito.getSueldo());
	}

	@Test
	public void testSetEdad() {
		empleadoPepito.setEdad(50);
		assertEquals(50, empleadoPepito.getEdad());
	}

	@Test
	public void testSetPuesto() {
		empleadoPepito.setPuesto(new PuestoTrabajo("Camarero"));
		assertEquals(new PuestoTrabajo("Camarero"), empleadoPepito.getPuesto());
	}

	// Tests con dátos no válidos

	@Test
	public void testSetNombreNoValido() {
		empleadoPepito.setNombre("Pepe Muñ0z S4nchez");
		assertNotEquals("Pepe Muñ0z S4nchez", empleadoPepito.getNombre());
	}

	@Test
	public void testSetSueldoNoValido() {
		empleadoPepito.setSueldo(500);
		assertNotEquals(500, empleadoPepito.getSueldo());
	}

	@Test
	public void testSetEdadNoValido() {
		empleadoPepito.setEdad(100);
		assertNotEquals(100, empleadoPepito.getEdad());
	}

	// Tests con datos nulos.

	@Test
	public void testSetNombreNulo() {
		try {
			empleadoPepito.setNombre(null);
			assertNotNull(empleadoPepito.getNombre());
			fail("No debe llegar aquí...");
		} catch (AssertionError e) {
			assertNotNull(empleadoPepito.getNombre());
		}
	}

	@Test
	public void testSetPuestoNulo() {
		try {
			empleadoPepito.setPuesto(null);
			assertNotNull(empleadoPepito.getPuesto());
			fail("No debe llegar aquí...");
		} catch (AssertionError e) {
			assertNotNull(empleadoPepito.getPuesto());
		}
	}

	// Test del método toString.

	@Test
	public void testEmpleadoToString() {
		assertEquals(
				"Empleado\n" + "nombre=Pepito Cocinillas López\n" + "sueldo=15000\n" + "edad=20\n" + "puesto=Chef\n",
				empleadoPepito.toString());
	}

}
