/** Piramide2.java
 *  Programa que dibuja una pirámide de un tamaño indicado por el usuario.
 *  mfp - 2018.11.08
 */

import java.util.Scanner;

public class Piramide2 {

	public static void main(String[] args) {

		System.out.println("Vamos a dibujar en pantalla una pirámide del número de líneas que indiques.");
		System.out.println("Indique el tamaño:");
		
		// Escribimos en pantalla el retorno del método "piramide" pasandole como argumento
		// un tamaño introducido desde una entrada de teclado.
		System.out.println(piramide(Integer.valueOf(new Scanner(System.in).nextLine())));
		
	}
	
	// Método que creará la pirámide linea por linea.
	static String piramide(int tamaño) {
		
		String piramideADibujar = "\n";

		for (int i = 0; i < tamaño; i++) {
			// Contatenamos el texto a devolver.
			piramideADibujar = piramideADibujar + (repetir(" ", tamaño - i) + repetir("*", i) + "*" + repetir("*", i) + "\n");
		}
		return piramideADibujar;
	}
	
	// Método que creará los carácteres necesarios basandose en repetición del caracter.
	static String repetir(String texto, int veces) {

		String textoResultante = "";
		
		for (int n = 1; n <= veces; n++) {
			textoResultante = textoResultante + texto;  
		}
		
		return textoResultante;
	}

}

