/** Fibonacci.java
 *  Programa que muestra el número en la posición indicada en la secuencia de Fibonacci.
 *  mfp - 2018.11.08
 */

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {

		System.out.println("Introduzca el número de posición en la secuencia de Fibonacci:");
		// Almacenamos temporalmente el resultado del cálculo directamente desde la llamada al método
		// a la vez que se usa la entrada de teclado como argumento.  
		long x = fibonacci(Integer.valueOf(new Scanner(System.in).nextLine()));

		// Si el resultado es -1 (Valor no válido) se muestra un error.
		if (x == -1) {
		
			System.out.println("Debe introducir un número entero positivo.");
		
		} else {
			
			System.out.println("Resultado: " + x);

		}
	}
	
	static long fibonacci(int posicion) {

		// Posiciones 0 y 1 de la secuencia de Fibonacci son resultados directos, para el resto se calcula.
		if (posicion == 0) {

			return 0;
			
		} else if (posicion == 1) {
			
			return 1;
	
		} else if (posicion > 1) {

			long posicionPrev1 = 0;
			long posicionPrev2 = 1;
			long posicionActual = 0;
			
			for (int i = 2; i <= posicion; i++) {
				
				posicionActual = posicionPrev1 + posicionPrev2;
				posicionPrev1 = posicionPrev2;
				posicionPrev2 = posicionActual;
				
			}
			
			return posicionActual;
			
		} else {
			// Si el número introducido es un valor no válido, se devuelve "-1" para indicar que el valor es no válido.
			return -1;
		}
		
	}
		
}


