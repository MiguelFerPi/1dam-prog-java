/** EcuacionSegundoGrado2.java
 *  Programa que calcula una ecuación de segundo grado.
 *  mfp - 2018.11.08
 */

import java.util.Scanner;

public class EcuacionSegundoGrado2 {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);

		System.out.println("Ahora vamos a calcular una ecuación de segundo grado con 3 números que proveerá.");
		System.out.print("Introduzca el valor de \"a\":");
		double a = Double.valueOf(teclado.nextLine().replace(',', '.'));
		System.out.print("Introduzca el valor de \"b\":");
		double b = Double.valueOf(teclado.nextLine().replace(',', '.'));
		System.out.print("Introduzca el valor de \"c\":");
		double c = Double.valueOf(teclado.nextLine().replace(',', '.'));

		System.out.println("");

		// Devolvemos la respuesta adecuada según el tipo de discriminante que tenga.
		if (evaluarDiscriminante(a,b,c) == 1) {
			// Degenerada
			System.out.println("La ecuación es degenerada.");
		} else if (evaluarDiscriminante(a,b,c) == 2){
			// Raíz única
			System.out.println("Resultado: x="+ x0);
		} else if (evaluarDiscriminante(a,b,c) == 3) {
			// Raíces real
			System.out.println("Resultado: x1="+ x1 +"; x2="+ x2);
		} else if (evaluarDiscriminante(a,b,c) == 4){
			// Raíces complejas		
			System.out.println("Resultado:\nx1="+ x +" + "+ y +"*i\nx2="+ x +" - "+ y +"*i");
		} else {
			System.out.println("Ha habido un error.");
		}


	}
	
	// Declaramos las variables globales a usar y asignamos un valor por defecto.
	static double x0 = 0;
	static double x1 = 0;
	static double x2 = 0;
	static double x = 0;
	static double y = 0;
	
	static int evaluarDiscriminante(double a, double b, double c) {

		if (a == 0 & b == 0) {
			// Degenerada
			return 1;
		} else if (a == 0) {
			// Raíz única con valor –c / b.
			x0 = -c / b;
			
			return 2;
		} else {
			double d = Math.pow(b, 2) - 4 * a * c;
			x = -b / 2 * a ;
			
			if (d >= 0) {
				// dos raíces reales x1 y x2
				y = Math.sqrt(d) / 2 * a;
				x1 = x + y;
				x2 = x - y;
				return 3;
			} else {
				// dos raíces complejas x1 y x2
				y = Math.sqrt(Math.abs(d)) / 2 * a;
				return 4;
			}
		}
		
	}

}

