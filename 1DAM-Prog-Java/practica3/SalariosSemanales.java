/** SalariosSemanales.java
 *  Programa que calcula el salario semanal de los trabajadores.
 *  mfp - 2018.11.06
 */

import java.util.Scanner;

public class SalariosSemanales {
	public static void main(String[] args) {

		System.out.println("Programa de cálculo de sueldos semanales.\n\n"
						 + "Por favor, introduzca los siguientes datos.\n");

		while (true) {
			// Llamada a todos los métodos usando unos como argumentos de otros.
			mostrarResultado(calcularSalario(pedirHoras()));
			
			if (deseaContinuar()) {	// Si la confirmación es "true" se rompe el bucle.
				break;
			}
			
		}

		System.out.println("Adios.");	// Mensaje de despedida.
	}

	// Pide por consola el número de horas que ha trabajado el trabajador.
	static int pedirHoras() {
		System.out.print("Número de horas trabajadas: ");
		
		return Integer.valueOf(new Scanner(System.in).nextLine()); // Devuelve lo introducido por consola.
	}
	
	// Realiza los cálculos del salario basado en el número de horas introducidas.
	static int calcularSalario(int horas) {
		int sueldoHora = 15;		// Sueldo regular por hora.
		int sueldoExtra = 22;		// Sueldo por cada hora extra.
		int horasRegulares = 35;	// Número de horas desde el que se considerarán extras.
		
		int horasExtra = 0;
		if (horas > horasRegulares) {	// Si hay horas extra, se separan en 2 variables para calcularlas separadamente.
			horasExtra = horas - horasRegulares;
			horas = horasRegulares;
		}

		int salario = horas*sueldoHora + horasExtra*sueldoExtra; // Cálculo del salario final.
		
		return salario;	// Devuelve el salario final.
	}
	
	// Muestra el salario introducido por pantalla.
	static void mostrarResultado(int salario) {
		System.out.println("El trabajador cobrará: "+ salario + "€.\n");
	}
	
	// Pregunta por la continuación del programa.
	static boolean deseaContinuar() {
		System.out.println("Si desea salir, pulse \"S\", si desea continuar, pulse Intro. ");
		
		// Devuelve "true" o "false" dependiendo del carácter introducido.
		return new Scanner(System.in).nextLine().toLowerCase().equals("s");
	}

}

