/** Banner.java
 *  Programa que usa un método para mostrar mis iniciales.
 *  mfp - 2018.11.08
 */

public class Banner {

	public static void main(String[] args) {

		banner();	// Llama al método que dibuja los iniciales.
	
	}

	// Dibuja las iniciales.
	static void banner() {
		System.out.println("\n\n");
		System.out.println("\t\t\t"+"M      M   FFFFFFF   PPPPPP ");
		System.out.println("\t\t\t"+"M M  M M   F         P     P");
		System.out.println("\t\t\t"+"M  MM  M   FFFFF     P     P");
		System.out.println("\t\t\t"+"M      M   F         PPPPPP ");
		System.out.println("\t\t\t"+"M      M   F         P      ");
		System.out.println("\t\t\t"+"M      M   F         P      ");
		System.out.println("\t\t\t"+"M      M   F         P      ");
	}
}

