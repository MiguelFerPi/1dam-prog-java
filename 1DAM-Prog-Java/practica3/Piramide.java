/** Piramide.java
 *  Programa que dibuja una pirámide de un tamaño indicado por el usuario.
 *  mfp - 2018.11.08
 */

import java.util.Scanner;

public class Piramide {

	public static void main(String[] args) {

		System.out.println("Vamos a dibujar en pantalla una pirámide del número de líneas que indiques.");
		System.out.println("Indique el tamaño:");
		
		// Llamamos al método que creará y mostrará en pantalla el dibujo de la pirámide,
		// usando una entrada de teclado como argumento.
		piramide(Integer.valueOf(new Scanner(System.in).nextLine()));
		
	}
	
	// Método que creará la pirámide linea por linea.
	static void piramide(int tamaño) {
		
		System.out.println("\n");
		for (int i = 0; i < tamaño; i++) {
			System.out.println(repetir(" ", tamaño - i) + repetir("*", i) + "*" + repetir("*", i) );
		}
	}
	
	// Método que creará los carácteres necesarios basandose en repetición del caracter.
	static String repetir(String texto, int veces) {

		String textoResultante = "";
		
		for (int n = 1; n <= veces; n++) {
			textoResultante = textoResultante + texto;  
		}
		
		return textoResultante;
	}

}

