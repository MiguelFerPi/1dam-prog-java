/** Rombo.java
 *  Programa que dibuja un rombo de un tamaño indicado por el usuario.
 *  mfp - 2018.11.08
 */

import java.util.Scanner;

public class Rombo {

	public static void main(String[] args) {

		System.out.println("Vamos a dibujar en pantalla un rombo del número de líneas que indiques.");
		System.out.println("Indique el tamaño con un número entero:");
		
		// Llamamos al método que creará y mostrará en pantalla el dibujo del rombo,
		// usando una entrada de teclado como argumento.
		rombo(Integer.valueOf(new Scanner(System.in).nextLine()));
		
	}
	
	// Método que creará el rombo linea por linea.
	static void rombo(int tamaño) {

		System.out.println("\n");
		for (int i = 0; i < tamaño; i++) {

			int m = 0;
			if (i == 0 || (double) tamaño / i > 2.0) {
				m = i;
			} else {
				m = tamaño -1 - i;
			}
			System.out.println(repetir(" ", tamaño - m) + repetir("*", m) + "*" + repetir("*", m) );
		}
	}
	
	// Método que creará los carácteres necesarios basandose en repetición del caracter.
	static String repetir(String texto, int veces) {

		String textoResultante = "";
		
		for (int n = 1; n <= veces; n++) {
			textoResultante = textoResultante + texto;  
		}
		
		return textoResultante;
	}

}

