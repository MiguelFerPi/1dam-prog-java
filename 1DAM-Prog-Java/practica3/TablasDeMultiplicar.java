/** TablasDeMultiplicar.java
 *  Programa que muestra la tabla de multiplicar del número introducido.
 *  mfp - 2018.11.07
 */

import java.util.Scanner;

public class TablasDeMultiplicar {

	public static void main(String[] args) {

		System.out.print("\nVamos a mostrar la tabla de multiplicar de un número,\n"
						+ "introduzca el número:");
		// Llamada al método que calculará y mostrará la tabla de multiplicar usando una entrada de teclado como argumento.
		calcularMostrarTabla(Integer.valueOf(new Scanner(System.in).nextLine()));
		
	}

	// Calculará y mostrará la tabla de multiplicar del número introducido.
	static void calcularMostrarTabla(int multiplo) {
		
		System.out.println("\n\t Tabla de múltiplicar del \""+ multiplo + "\"");
		
		for (int i = 0;i <= 15; i++) {
			// Formato "Multiplo x Indice del bucle = Resultado" separado por tabulaciones.
			System.out.println("\t"+ multiplo +"\tx\t"+ i +"\t=\t"+ multiplo*i);
			
		}

	}
}

