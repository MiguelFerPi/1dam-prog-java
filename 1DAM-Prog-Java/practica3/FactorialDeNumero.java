/** FactorialDeNumero.java
 *  Programa que calcula el factorial de un número introducido.
 *  mfp - 2018.11.07
 */

import java.util.Scanner;

public class FactorialDeNumero {

	public static void main(String[] args) {

		System.out.print("\nVamos a mostrar el factorial de un número introducido,\n"
				+ "introduzca el número: ");
		
		// Muestra en pantalla el resultado devuelto por "factorial" que a su vez usa una entrada de teclado como argumento.
		System.out.println("Resultado: "+ factorial(Integer.valueOf(new Scanner(System.in).nextLine())));
		
	}

	static long factorial(int base) {
		
		long resultado = 1;
		// Se calcula el factorial con un bucle for.
		for (int i = 1; i <= base; i++) {
			resultado = resultado * i;
		}
		
		return resultado;
		
	}
	
}

