
/**
 * Multiplosde3.java
 * Programa que usa muestra todos los múltiplos de 3 hasta el 3000
 * mfp - 2018.10.18
 */

public class Multiplosde3 {
	public static void main(String[] args) {

		// Mostramos un poco de introducción.
		System.out.println("Vamos a mostrar todos los múltiplos de 3 hasta el 3000.");

		// Iniciamos el bucle con el que calcularemos y mostraremos los resultados.
		for (int i = 0; i <= 3000; i = i + 3) {
			System.out.println(i);
		}
	}
}
