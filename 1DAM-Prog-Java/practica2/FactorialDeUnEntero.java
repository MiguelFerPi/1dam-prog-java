/** FactorialDeUnEntero.java
 *  Programa que pide un número entero positivo y muestra su factorial.
 *  mfp - 2018.10.30
 */

import java.util.Scanner;

public class FactorialDeUnEntero {

	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.print("Introduzca un número entero positivo y le mostraré su factorial:");
		
		// Declaramos las variables a usar a la vez que pedimos el número con el que trabajar.
		long numero = Integer.valueOf(teclado.nextLine());
		long resultado = 1;
		
		// Bucle que calculará el factorial.
		for (int i = 1; i <= numero; i++) {
			resultado = resultado * i;
		}
		
		System.out.println("El factorial de " + numero + " es: " + resultado);
	}

}

