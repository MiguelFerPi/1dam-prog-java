/** EcuacionSegundoGrado.java
 *  Programa que calcula una ecuación de segundo grado.
 *  mfp - 2018.10.30
 */

import java.util.Scanner;

public class EcuacionSegundoGrado {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);

		System.out.println("Ahora vamos a calcular una ecuación de segundo grado con 3 números que proveerá.");
		System.out.print("Introduzca el valor de \"a\":");
		double a = Double.valueOf(teclado.nextLine().replace(',', '.'));
		System.out.print("Introduzca el valor de \"b\":");
		double b = Double.valueOf(teclado.nextLine().replace(',', '.'));
		System.out.print("Introduzca el valor de \"c\":");
		double c = Double.valueOf(teclado.nextLine().replace(',', '.'));

		// Declaramos las variables a usar y asignamos un valor por defecto.
		double x0 = 0;
		double x1 = 0;
		double x2 = 0;
		double x = 0;
		double y = 0;
	
		if (a == 0 & b == 0) {
			// Degenerada
		} else if (a == 0) {
			// Raíz única con valor –c / b.
			x0 = -c / b;
		} else {
			double d = Math.pow(b, 2) - 4 * a * c;

			x = -b / 2 * a ;
			
			if (d >= 0) {
				// dos raíces reales x1 y x2
				y = Math.sqrt(d) / 2 * a;
				x1 = x + y;
				x2 = x - y;
			} else {
				// dos raíces complejas x1 y x2
				y = Math.sqrt(Math.abs(d)) / 2 * a;
			}
		}
		
		System.out.println("");
		if (x0 == 0 && x1 == 0 && x == 0) {
			// Degenerada
			System.out.println("La ecuación es degenerada.");
		} else if (x0 != 0){
			// Raíz única
			System.out.println("Resultado: x="+ x0);
		} else if (x1 != 0) {
			// Raíces real
			System.out.println("Resultado: x1="+ x1 +"; x2="+ x2);
		} else {
			// Raíces complejas		
			System.out.println("Resultado:\nx1="+ x +" + "+ y +"*i\nx2="+ x +" - "+ y +"*i");
		}


	}

}

