
/**
 * Patron.java
 * Programa que muestra un dibujo de algo que no tengo idea de que es, un pikachu o algo.
 * mfp - 2018.10.11
 */


public class Patron {
	public static void main(String[] args) {
		// Mostramos en pantalla el dibujo de un bicho raro.
		System.out.println("        |\\_/|");
		System.out.println("       / @ @ \\");
		System.out.println("      ( > º < )");
		System.out.println("       `»»x««´");
		System.out.println("       /  O  \\");
	}
}