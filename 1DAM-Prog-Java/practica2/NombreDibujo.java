
/**
 * NombreDibujo.java
 * Programa que muestra mis iniciales como un dibujo.
 * mfp - 2018.10.11
 */


public class NombreDibujo {
	public static void main(String[] args) {
		// Mostramos en pantalla mis iniciales en forma de dibujo.
		System.out.println("\n\n");
		System.out.println("\t\t\t"+"M      M   FFFFFFF   PPPPPP ");
		System.out.println("\t\t\t"+"M M  M M   F         P     P");
		System.out.println("\t\t\t"+"M  MM  M   FFFFF     P     P");
		System.out.println("\t\t\t"+"M      M   F         PPPPPP ");
		System.out.println("\t\t\t"+"M      M   F         P      ");
		System.out.println("\t\t\t"+"M      M   F         P      ");
		System.out.println("\t\t\t"+"M      M   F         P      ");
	}
}