

import java.util.Scanner;

public class Cuadrado {
	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Ahora dibujaremos un cuadrado de acuerdo a sus especificaciones de tamaño.");
		
		System.out.print("Introduzca tamaño del lado: ");
		int lado = teclado.nextInt();
		
		// Bucle para dibujar de arriba a abajo.
		for (int i = 1; i <= lado; i++) {

			// Si es la primera o última linea, lados de arriba y abajo, entra en un bucle para hacer la linea completa.			
			if (i == 1 | i == lado) {
				for (int j = 1; j <= lado; j++) {
					System.out.print(" *");
				}
				System.out.print("\n");
				
			} else {
				/* Si es una linea del interior del cuadrado,
				 * entra en un bucle con una variable que contiene una condición
				 * para saber si debe tener contenido o crear un hueco vacío.
				 */
				for (int j = 1; j <= lado; j++) {
					String interior = (j == 1 | j == lado) ? " *" : "  ";

					System.out.print(interior);
				}
				System.out.print("\n");
				
			}
			
		}
		
	}
}
