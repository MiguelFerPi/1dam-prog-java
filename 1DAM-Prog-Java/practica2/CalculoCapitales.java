

// Importamos la función Scanner para capturar entradas en consola.
import java.util.Scanner;

public class CalculoCapitales {
	public static void main(String[] args) {
		// Se inicia el Scanner en la variable "teclado".
		Scanner teclado = new Scanner(System.in);
		
		// Se declaran las variables
		double capInicial;
		double tipoInteres;
		int numAños;
		double interes;
		double capFinal;
		
		// Pedimos al usuario que introduzca los datos necesarios,
		// limpiando el búfer antes de cada nuevo número.
		System.out.println("Por favor, introduzca los siguientes datos de su inversión:");

		System.out.print("Capital Inicial: ");
		capInicial = Double.valueOf(teclado.nextLine());
		
		teclado = new Scanner(System.in);
		System.out.print("Tipo de Interés: ");
		tipoInteres = Double.valueOf(teclado.nextLine());

		teclado = new Scanner(System.in);
		System.out.print("Número de años: ");
		numAños = Integer.valueOf(teclado.nextLine());

		// Ahora procedemos a los cálculos.
		// Cf = Ci * (1 + r)^n
		interes = Math.pow(1 + tipoInteres/100, numAños);
		capFinal = capInicial * interes;

		// Mostramos en pantalla el resultado de los cálculos.
		System.out.println("\nSu inversión a producido un interés de:");
		System.out.println(capFinal-capInicial + "€");
		System.out.println("Resultando en un Capital final de:");
		System.out.println(capFinal+"€");
	}
}