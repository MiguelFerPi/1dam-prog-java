

import java.util.Scanner;

public class RedondearNumero {
	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);

		System.out.println("Este programa redondeará un número con decimales según sus indicaciones.");
		
		while (true) {
			System.out.print("\nNúmero a redondear (0 para finalizar): ");
			/* Almacenamos el número decimal introducido, valueOf es necesario porque 
			 * teclado.nextDouble tiene problemas con el separador de decimales.
			 */      
			double numDecimal = Double.valueOf(teclado.nextLine().replace(',', '.'));
																	
			teclado = new Scanner(System.in);

			if (numDecimal == 0) break;	// Si el número introducido era un 0, terminamos el bucle.
			
			System.out.print("\nNúmero de decimales deseado: ");
			int decimales = teclado.nextInt();	// Almacenamos el número de decimales.
			teclado = new Scanner(System.in);
			// Movemos el separador de decimales tantas posiciones como se necesita, se redondea y despues se mueve otra vez.
			numDecimal = Math.rint(numDecimal * Math.pow(10, decimales)) / Math.pow(10, decimales);
			
			System.out.println("\nNúmero redondeado: " + numDecimal);
		}
		
	}
}
