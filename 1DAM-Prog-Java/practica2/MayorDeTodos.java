

import java.util.Scanner;

public class MayorDeTodos {
	public static void main(String[] args) {

		// Iniciamos Scanner.
		Scanner teclado = new Scanner(System.in);

		// Declaramos la variable LONG con el valor inicial de 0.
		long mayor = Long.MIN_VALUE;
		// Declaramos una variable LONG para almacenar temporalmente el último número introducido.
		long ultimo;
		// Declaramos una variable BOOLEAN con valor inicial de false, para usarla como método de control en el bucle.
		boolean bucleStop = false;
		
		// Indicamos al usuario qué vamos a pedir.
		System.out.println("Vamos a quedarnos con el número más grande de los que se introduzcan.");

		// Iniciamos el bucle con el que pediremos los números a comparar.
		for (int i = 1; !bucleStop; i++) {
			System.out.print(i+"º número: ");
			ultimo = Long.valueOf(teclado.nextLine()); // Almacenamos temporalmente el número introducido.
			teclado = new Scanner(System.in); // Limpiamos el búfer de Scanner.

			if (mayor < ultimo) // Si el número introducido es mayor que el almacenado, se reemplaza con el nuevo.
			{
				mayor = ultimo; 
			}
			System.out.println("El mayor número hasta ahora es: " + mayor + "\n");
			
			System.out.print("Pulse \"S\" si desea continuar. ");
			
			if (teclado.nextLine().toLowerCase().charAt(0) != 's'){
				bucleStop = true;
			}
			teclado = new Scanner(System.in); // Limpiamos el búfer de Scanner.
			System.out.println();
		}
	}
}
