import java.util.Scanner;

/** MenuCalculadora.java
 *  Programa de calculadora que ofrecerá las operaciones a realizar con un menú.
 *  mfp - 2018.10.30
 */

public class MenuCalculadora {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Bienvenido a la calculadora.");

		while (true) {
			System.out.println("\nSeleccione qué cálculo desea realizar (púlse el número de su opción):");
			System.out.println(" 1 - Suma");
			System.out.println(" 2 - Resta");
			System.out.println(" 3 - Multiplicación");
			System.out.println(" 4 - División");
			System.out.println(" ==================");
			System.out.println(" 0 - Salir");
			
			// Almacenamos temporalmente la opción seleccionada del menú.
			int opcion = Integer.valueOf(teclado.nextLine());
			teclado = new Scanner(System.in);
			
			if (opcion == 0) break;
			
			// Si la opción no está en el menú, devolverá un error.
			if (opcion > 4 | opcion < 0) {
				System.out.println("Opción inválida");
				continue;
			}
			
			System.out.println("Introduzca los valores a calcular.");

			System.out.print("Primer número: ");
			double num1 = Double.valueOf(teclado.nextLine());
			teclado = new Scanner(System.in);
			
			System.out.println("Segundo número: ");
			double num2 = Double.valueOf(teclado.nextLine());
			teclado = new Scanner(System.in);
		
			// Ejecutar la operación según la opción seleccionada.
			double resultado = 0;
			if (opcion == 1) resultado = num1 + num2;
			if (opcion == 2) resultado = num1 - num2;
			if (opcion == 3) resultado = num1 * num2;
			if (opcion == 4) resultado = num1 / num2;
			
			System.out.println("El resultado es: " + resultado + "\n");
						
		}
		
		System.out.println("\nAdiós.");
		
	}

}

