
/**
 * DatosPersonales.java
 * Programa simple que muestra mis datos personales.
 * mfp - 2018.10.11
 */


public class DatosPersonales {
	public static void main(String[] args) {
		// Se declaran las variables de tipo STRING.
		String nombre;
		String apellidos;
		String calle;
		String ciudad;
		String pais;
		int cpostal;

		// Asignamos los valores a las anteriores variables.
		nombre = "Miguel";
		apellidos = "Fernández Piñero";
		calle = "Primero de Mayo";
		ciudad = "El Palmar";
		pais = "España";
		cpostal = 30120;

		// Mostramos en pantalla el valor de las variables con un poco de texto para adornar.
		System.out.println("Buenas,");
		System.out.println("soy un programa escrito por " + nombre + " " + apellidos+".");
		System.out.println("Que vive en la calle " + calle + ", en " + ciudad);
		System.out.print("con código postal " + cpostal + ", del país de " + pais + ".");
	}
}