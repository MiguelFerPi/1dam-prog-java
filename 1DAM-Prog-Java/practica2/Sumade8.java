

import java.util.Scanner;

public class Sumade8 {
	public static void main(String[] args) {

		// Iniciamos Scanner.
		Scanner teclado = new Scanner(System.in);

		// Declaramos la variable LONG con el valor inicial de 0.
		long suma = 0;
		
		// Indicamos al usuario qué vamos a pedir.
		System.out.println("Vamos a calcular la suma de 8 números que debes introducir.");

		// Iniciamos el bucle con el que pediremos los números a sumar.
		for (int i = 1; i <= 8; i++) {
			System.out.print(i+"º número: ");
			suma += Long.valueOf(teclado.nextLine()); // Sumamos directamente el número proporcionado al que tuvieramos anteriormente.
			teclado = new Scanner(System.in); // Limpiamos el búfer de Scanner.
		}
		System.out.println("Total suma: " + suma + "\n");
	}
}