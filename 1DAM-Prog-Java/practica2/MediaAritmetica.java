

import java.util.Scanner;

public class MediaAritmetica {
	public static void main(String[] args) {

		// Iniciamos Scanner.
		Scanner teclado = new Scanner(System.in);

		// Declaramos la variable LONG con el valor inicial de 0.
		long suma = 0;
		// Declaramos una variable LONG para almacenar temporalmente el último número introducido.
		long ultimo;
		// Declaramos una variable DOUBLE para calcular la media.
		double media;
		// Declaramos una variable INT con valor inicial 0 para usarla como contador para la media aritmética.
		int i = 0;
		
		// Indicamos al usuario qué vamos a pedir.
		System.out.println("Introduzca números positivos para realizar los cálculos.");
		System.out.println("Introducir 0 finalizará el programa.\n");

		// Iniciamos el bucle con el que pediremos los números a para calcular,
		// y usaremos el índice del bucle como variable para calcular la media aritmética.
		while (true) {
			i++; // Añadimos 1 al contador de la media. 
			System.out.print(i+"º número: ");
			ultimo = Long.valueOf(teclado.nextLine()); // Almacenamos temporalmente el número introducido.
			teclado = new Scanner(System.in); // Limpiamos el búfer de Scanner.
			suma = suma + ultimo;
			media = suma/i;
			
			if (ultimo <= 0) {	// Si el número introducido es menor o igual a 0, el bucle y el programa se detienen.
				break;
			}

			// Se muestran los resultados.
			System.out.println("La suma actual es: " + suma);
			System.out.println("La media aritmética es: " + media + "\n");
		}
	}
}
