
/**
 * TresVariables.java
 * Programa simple que declara 3 variables INT y las muestra.
 * mfp - 2018.10.11
 */

public class TresVariables {
	public static void main(String[] args) {
		// Se declaran las 3 variables de tipo INT.
		int var1;
		int var2;
		int var3;

		// Les asignamos 3 valores distintos respectivamente.
		var1 = 5;
		var2 = 9;
		var3 = 13;
		
		// Mostramos en pantalla el valor de las variables con un poco de texto para adornar.
		System.out.println("Buenas,");
		System.out.println("ahora te voy a mostrar 3 números, aquí van:");
		System.out.println("1º Número: " + var1);
		System.out.println("2º Número: " + var2);
		System.out.println("3º Número: " + var3);
	}
}