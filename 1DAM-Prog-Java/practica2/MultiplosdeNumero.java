

import java.util.Scanner;

public class MultiplosdeNumero {
	public static void main(String[] args) {
		
		// Iniciamos Scanner.
		Scanner teclado = new Scanner(System.in);

		// Declaramos las variables de los parámetros que pediremos al usuario.
		long multiplo;
		long maximo;

		// Mostramos lo que vamos a pedir y hacer
		System.out.println("Vamos a mostrar todos los múltiplos del número que introduzca hasta otro que tambien nos indicará.\n");
		System.out.print("Introduzca el múltiplo a calcular: ");
		multiplo = Long.valueOf(teclado.nextLine()); // Almacenamos el número.
		teclado = new Scanner(System.in); // Limpamos búfer de teclado.
		System.out.println("");
		
		System.out.print("Introduzca el máximo a calcular: ");
		maximo = Long.valueOf(teclado.nextLine()); // Almacenamos el número.
		teclado = new Scanner(System.in); // Limpamos búfer de teclado.
		System.out.println("\n");
		
		// Iniciamos el bucle con el que calcularemos y mostraremos los resultados.
		for (long i = 0; i <= maximo; i = i + multiplo) {
			System.out.println(i);
		}
	}
}
