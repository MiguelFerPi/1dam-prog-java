

// Importamos la función Scanner para capturar entradas en consola.
import java.util.Scanner;

public class TresDatos {
	public static void main(String[] args) {
		// Se inicia el Scanner en la variable "teclado".
		Scanner teclado = new Scanner(System.in);
		
		// Se declaran las 3 variables de tipo INT.
		int var1;
		int var2;
		int var3;

		// Pedimos al usuario que introduzca 3 números enteros, limpiando el búfer antes de cada nuevo número.
		System.out.println("Por favor, introduzca 3 números enteros a continuación:");

		System.out.print("1º número: ");
		var1 = Integer.valueOf(teclado.nextLine());
		
		teclado = new Scanner(System.in);
		System.out.print("2º número: ");
		var2 = Integer.valueOf(teclado.nextLine());

		teclado = new Scanner(System.in);
		System.out.print("3º número: ");
		var3 = Integer.valueOf(teclado.nextLine());
				
		// Mostramos en pantalla el valor de las variables con un poco de texto para adornar.
		System.out.println("\nAhora te voy a repetir los 3 números que me has proporcionado, aquí van:");
		System.out.println("1º Número: " + var1);
		System.out.println("2º Número: " + var2);
		System.out.println("3º Número: " + var3);
	}
}