
/**
 * PruebaIdentificadores.java
 * Programa ejemplo que muestra los identificadores válidos en Java
 * ajp - 2018.09.19
 */

public class PruebaIdentificadores {
	@SuppressWarnings("unused")
	public static void main(String[] args) {	
		int _alpha;         //Sí
		int FLOAT;          //Sí
		//int 1_de_muchos;  //No, no puede empezar por un número
		int maxValor;       //Sí
		int cuantos;        //Sí
		//int "dado";       //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
		int Nbytes;         //Sí
		//int pink.panter;  //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
		//int int;          //No, el identificador no puede ser un nombre reservado. 
		//int qué_dices?;   //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
		int Número;         //Sí
		//int cadena 2;     //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
		int Cañón;          //Sí
		int café;           //Sí
		//int Mesa-3;       //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
		int Return;         //Sí
		int While;          //Sí
		int __if;           //Sí
		//int Bloque#4;     //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
		//int c o s a;      //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
		int _CaPrIcHoSo_;   //Sí
		//int 8ªRoca;       //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
		//int 3d2;          //No, no puede empezar por un número
		//int Hoja3/5;      //No, sólo puede contener caracteres alfanuméricos, guión bajo y signo de dólar
	}
}