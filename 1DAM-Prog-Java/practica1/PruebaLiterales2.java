
/**
 * PruebaLiterales2.java
 * Programa ejemplo que muestra los literales válidos en Java
 * ajp - 2018.09.19
 */

public class PruebaLiterales2 {
	@SuppressWarnings({ "unused" })
	public static void main(String[] args) {	
		double uno = Double.valueOf("-11.1");        
		double dos = Double.valueOf("-88.28");        
		double tres = Double.valueOf(".3e27");        
		double cuatro = Double.valueOf("23e2.3");    
		String cinco = "cañón";        
		int seis = 0377;               
		int siete = 9999;              
		int ocho = Integer.valueOf("099");                
		double nueve = Double.valueOf(+521.6);      
		double diez = Double.valueOf(1.26);         
		double once = Double.valueOf(5E-002);             
		int doce = 0xFE;                  
		int trece = 0b101010;             
		double catorce = Double.valueOf(1.26f);     
		char quince = Character.valueOf('\n');         
		String dieciseis = "while";						//Necesitó comillas ya que "while" es un nombre reservado   
		String diecisiete = "\\xFE";					//Necesitó escapar la barra invertida ya que \x no es un valor de escape válido.						
		int dieciocho = 1234;       
		double diecinueve = Double.valueOf(.567);         
		int veinte = 0xFFFE;            
		String veintiuno = "XGA";						//Necesitaba ponerlo entre comillas dobles.         
		String veintidos = "a";         
		String veintitres = "abc";						//abc sólo podía ser un string ya que aunque estaba entre comillas simples, no es un sólo caracter.         
		double veinticuatro = Double.valueOf(02.45);      
		char veinticinco = 'a';         
		String veintiseis = "xF2E";       				//Necesitaba ponerlo entre comillas dobles.
		int veintisiete = 0xf;          
		String veintiocho = "abc";        				//Necesitaba ponerlo entre comillas dobles.
		String veintinueve = "ab\"c";					//Necesitaba ponerlo entre comillas dobles y escapar las dobles comillas si es que se desea que sean parte del valor.      
		String treinta = "abc";							//Le faltaba unas dobles comillas.
		String treintayuno = "abc";						//Comillas simples y dobles no son intercambiables, había que reemplazar la simple con dobles.     
		char treintaydos = 'a';							//Le faltaba una comilla simple.          
		String treintaytres = "abc;";   
		String treintaycuatro = "abc'"; 
		String treintaycinco = "abc";					//Solo se pone una serie de comillas dobles para el valor, si no intentará leer el valor en el lugar equivocado.  
		String treintayseis = "abc";					//Solo se pone una serie de comillas dobles para el valor, si no intentará leer el valor en el lugar equivocado.  
		String treintaysiete = "true";
		boolean treintayocho = Boolean.valueOf("True");    
		boolean treintaynueve = false;
		char cuarenta = '\\';           
	}
}
