

import java.util.Scanner;

public class Nombre1 {
	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		System.out.print("\nNombre 1º persona: ");
		String persona1Nombre = teclado.nextLine();
		teclado = new Scanner(System.in);
		System.out.print("\nEdad 1º persona: ");
		int persona1Edad = Integer.valueOf(teclado.nextLine());
		teclado = new Scanner(System.in);
		System.out.print("\nNombre 2º persona: ");
		String persona2Nombre = teclado.nextLine();
		teclado = new Scanner(System.in);
		System.out.print("\nEdad 2º persona: ");
		int persona2Edad = Integer.valueOf(teclado.nextLine());
		
		System.out.println("\n\nNombre\tEdad");
		System.out.println(persona1Nombre + "\t" + persona1Edad);
		System.out.println(persona2Nombre + "\t" + persona2Edad);

	}
}