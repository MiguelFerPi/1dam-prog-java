

import java.util.Scanner;

public class MayorDeDos {
	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		System.out.print("\nPrimer número: ");
		int numero1 = Integer.valueOf(teclado.nextLine());
		
		teclado = new Scanner(System.in);
		System.out.print("\nSegundo número: ");
		int numero2 = Integer.valueOf(teclado.nextLine());

		if (numero1 > numero2) {
			System.out.println("\n\nEl número más alto es: " + numero1);
		} else if (numero1 < numero2) {
			System.out.println("\n\nEl número más alto es: " + numero2);
		} else {
			System.out.println("\n\nAmbos números son iguales.");
		}
		
		
	}
}