
/**
 * PruebaLiterales.java
 * Programa ejemplo que muestra los literales válidos en Java
 * ajp - 2018.09.19
 */

public class PruebaLiterales {
	@SuppressWarnings("unused")
	public static void main(String[] args) {	
		double uno = -11.1;               //Sí
		double dos = -88.28;              //Sí
		double tres = .3e27;              //Sí
		//double cuatro = 23e2.3;         //No, el formato es erróneo para cualquier tipo de variable.
		String cinco = "cañón";           //Sí
		int seis = 0377;                  //Sí
		int siete = 9999;                 //Sí
		//int ocho = 099;                 //No, el formato es erróneo para cualquier tipo de variable.
		double nueve = +521.6;            //Sí
		double diez = 1.26;               //Sí
		double once = 5E-002;             //Sí
		int doce = 0xFE;                  //Sí
		int trece = 0b101010;             //Sí
		double catorce = 1.26f;           //Sí
		//char quince = ‘\n’;             //No, char usa comilla simple y no tildes.
		//String dieciseis = while;       //No, String necesita encapsular el valor con dobles comillas.
		//int diecisiete = \xFE;          //No, el formato es erróneo para cualquier tipo de variable.
		int dieciocho = 1234;             //Sí
		double diecinueve = .567;         //Sí
		int veinte = 0xFFFE;              //Sí
		//String veintiuno = XGA;         //No, String necesita encapsular el valor con dobles comillas.
		String veintidos = "a";           //Sí
		//char veintitres = 'abc';        //No
		double veinticuatro = 02.45;      //Sí
		char veinticinco = 'a';           //Sí
		//String veintiseis = xF2E;       //No, String necesita encapsular el valor con dobles comillas.
		int veintisiete = 0xf;            //Sí
		//String veintiocho = abc;        //No, String necesita encapsular el valor con dobles comillas.
		//String veintinueve = ab"c;      //No, String necesita encapsular el valor con dobles comillas.
		//String treinta = "abc;          //No, String necesita encapsular el valor con dobles comillas.
		//String treintayuno = "abc';     //No, String necesita encapsular el valor con dobles comillas.
		//char treintaydos = a';          //No
		String treintaytres = "abc;";     //Sí
		String treintaycuatro = "abc'";   //Sí
		//String treintaycinco = "abc"";  //No, String necesita encapsular el valor con dobles comillas.
		//String treintayseis = ""abc"";  //No, String necesita encapsular el valor con dobles comillas una vez.
		String treintaysiete = "true";    //Sí
		//boolean treintayocho = True;    //No, la constante true debe ser escrita en minúsculas.
		boolean treintaynueve = false;    //Sí
		char cuarenta = '\\';             //Sí
	}
}