
/**
 * HolaMundo.java
 * Programa ejemplo que muestra un mensaje simple por pantalla.
 * ajp - 2018.09.19
 */

public class HolaMundo {
	public static void main(String[] args) {	
		//Muestra un mensaje en la pantalla
		System.out.println("Hola mundo...");  
	}
}
