
/**
 * e1_InvertirTexto.java
 * Programa que invierte una cadena de texto introducida.
 * mfp - 2019.01.10
 */

import java.util.Scanner;

public class e1_InvertirTexto {

	public static void main(String[] args) {

		System.out.println("Introduzca un texto a invertir:");

		System.out.println(invertirTexto(new Scanner(System.in).nextLine()));

	}

	private static String invertirTexto(String texto) {

		StringBuilder textoIntroducido = new StringBuilder(texto);
		StringBuilder textoInvertido = new StringBuilder();
		do {

			textoInvertido.append(textoIntroducido.charAt(textoIntroducido.length() - 1));
			textoIntroducido.deleteCharAt(textoIntroducido.length() - 1);

		} while (textoIntroducido.length() > 0);

		return textoInvertido.toString();
	}

}
