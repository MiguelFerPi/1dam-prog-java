/** e7_ReemplazaPalabras.java
 *  Programa que reemplaza una serie de palabras de un texto con asteriscos.
 *  mfp - 2019.01.11
 */

public class e7_ReemplazaPalabras {

	public static void main(String[] args) {

		String texto = "Oracle ha anunciado hoy su nueva generación de compilador Java hoy. "
				+ "Utiliza analizador avanzado y optimizador especial para la JVM de Oracle";
		String palabras = "JVM,Java,Oracle";
		
		System.out.println(reemplazaPalabras(texto, palabras));
	}

	private static String reemplazaPalabras(String texto, String palabras) {

		char caracter = '*';

		String[] palabrasReemplazar = palabras.split(",");
		StringBuilder textoSB = new StringBuilder(texto);

		for (String palabra : palabrasReemplazar) {
			String reemplazo = "";
			for (int i = 0; i < palabra.length(); i++) {
				reemplazo += caracter;
			}

			while (textoSB.indexOf(palabra) != -1) {
				int indInicio = textoSB.indexOf(palabra);
				int indFinal = indInicio + palabra.length();

				textoSB.replace(indInicio, indFinal, reemplazo);
			}

		}

		return textoSB.toString();
	}

}
