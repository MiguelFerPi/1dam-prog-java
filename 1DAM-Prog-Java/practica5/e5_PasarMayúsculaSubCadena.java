
/**
 * e5_PasarMayúsculaSubCadena.java
 * Programa que pasa texto a mayúsculas según unas etiquetas.
 * mfp - 2019.01.10
 */

public class e5_PasarMayúsculaSubCadena {

	public static void main(String[] args) {

		String texto = "Estamos viviendo en un <mayus>submarino amarillo</mayus>. "
				+ "No tenemos <mayus>nada</mayus> qué hacer";

		System.out.println(PasarMayúsculaSubCadena(texto));

	}

	private static String PasarMayúsculaSubCadena(String texto) {
		StringBuilder textoSB = new StringBuilder(texto);

		String tagMayusInicio = "<mayus>";
		String tagMayusFinal = "</mayus>";

		int indice = 0;

		while (textoSB.indexOf(tagMayusInicio, indice) != -1) {
			// Extrae los índices de las etiquetas.
			int indiceInicial = textoSB.indexOf(tagMayusInicio, indice);
			int indiceFinal = textoSB.indexOf(tagMayusFinal, indiceInicial);

			// Extrae y convierte en mayúsculas el texto a convertir.
			String subcadenaMayus = textoSB.substring(indiceInicial + tagMayusInicio.length(), indiceFinal)
					.toUpperCase();

			// Reemplaza las etiquedas y el texto etiquetado con la convertida.
			textoSB.replace(indiceInicial, indiceFinal + tagMayusFinal.length(), subcadenaMayus);

		}

		return textoSB.toString();
	}

}
