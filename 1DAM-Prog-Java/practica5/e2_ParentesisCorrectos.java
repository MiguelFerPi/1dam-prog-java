
/** 
 * e2_ParentesisCorrectos.java
 * Programa que corrige los paréntesis de una expresión aritmética.
 * mfp - 2019.01.10
 */

import java.util.Scanner;

public class e2_ParentesisCorrectos {

	public static void main(String[] args) {

		System.out.println("Introduzca una expresión aritmética a comprobar:");

		if (parentesisCorrectos(new Scanner(System.in).nextLine())) {
			System.out.println("Los paréntesis son correctos.");
		} else {
			System.out.println("Los paréntesis son incorrectos.");
		}

	}

	private static boolean parentesisCorrectos(String expresionIntroducida) {

		StringBuilder expresion = new StringBuilder(expresionIntroducida);


		// Elimina todos los espacios de la expresión.
		for (int i = 0; i < expresion.length(); i++) {
			if (expresion.charAt(i) == ' ') {
				expresion.deleteCharAt(i);
				i--;
			}
		}

		byte numParentesis = 0;
		for (int i = 0; i < expresion.length(); i++) {

			if (expresion.charAt(i) == '(') {
				numParentesis++;
			} else if (expresion.charAt(i) == ')') {
				numParentesis--;
			}

			// Si en algún momento hay un cierre antes que una apertura devuelve falso.
			if (numParentesis < 0) {
				return false;
			}
			
			// Si el paréntesis está al lado de un operador devuelve falso.
			if (i > 0 && (expresion.charAt(i) == '(' || expresion.charAt(i) == ')') &&
				   (expresion.charAt(i - 1) == '+' ||
					expresion.charAt(i - 1) == '-' ||
					expresion.charAt(i - 1) == '*' ||
					expresion.charAt(i - 1) == '/')) {
				return false;
			}
			
			if (i < expresion.length() && (expresion.charAt(i) == '(' || expresion.charAt(i) == ')') &&
					   (expresion.charAt(i + 1) == '+' ||
						expresion.charAt(i + 1) == '-' ||
						expresion.charAt(i + 1) == '*' ||
						expresion.charAt(i + 1) == '/')) {
					return false;
			}
		}

		// Si el número de aperturas y cierres no es el mismo, es falso.
		if (numParentesis != 0) {
			return false;
		}

		return true;
	}

}
