
/**
 * e4_ObtenerNumeroVecesSubCadena2.java
 * Programa que cuenta el número de veces que un
 * texto se repite en otro sin distinguir mayúsculas.
 * mfp - 2019.01.10
 */

public class e4_ObtenerNumeroVecesSubCadena2 {

	public static void main(String[] args) {

		String texto = "Estamos viviendo en un submarino amarillo. "
				+ "No tenemos nada que hacer. "
				+ "En el interior del submarino se está muy apretado. "
				+ "Así que estamos leyendo todo el día. "
				+ "Vamos a salir en 5 días.";

		String subcadena = "en";

		System.out.println("Se repite " + obtenerNumeroVecesSubCadena2(texto.toLowerCase(), subcadena.toLowerCase()) + " veces.");

	}

	private static int obtenerNumeroVecesSubCadena2(String texto, String subcadena) {
		StringBuilder textoSB = new StringBuilder(texto);

		int indice = 0;
		int contador = 0;

		while (textoSB.indexOf(subcadena, indice) != -1) {
			contador++;
			indice = textoSB.indexOf(subcadena, indice) + 1;
			
		}
		
		return contador;
	}

}
