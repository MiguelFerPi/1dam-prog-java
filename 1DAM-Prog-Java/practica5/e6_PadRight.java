
/** e6_PadRight.java
 *  Programa que añadirá una serie de caracteres a un texto introducido hasta una longitud determinada.
 *  mfp - 2019.01.11
 */

import java.util.Scanner;

public class e6_PadRight {

	public static void main(String[] args) {

		System.out.println("Introduzca un texto:");

		char caracter = '*';
		int numero = 30;
		System.out.println(padRight(new Scanner(System.in).nextLine(), caracter, numero));
	}

	private static String padRight(String texto, char caracter, int numero) {

		int textoLong = texto.length();
		StringBuilder textoSB = new StringBuilder(texto);
		for (int i = 0; i <= numero - textoLong; i++) {
			textoSB.append(caracter);
		}

		return textoSB.toString();
	}

}
