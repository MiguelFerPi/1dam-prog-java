/** e9_Encriptar.java
 *  -
 *  mfp - 2019.01.11
 */

public class e9_Encriptar {

	public static void main(String[] args) {

		String texto = "Hola, esto es un mensaje de prueba.";
		String clave = "Pikolin";

		System.out.println(encriptar(texto, clave));

	}

	private static String encriptar(String texto, String clave) {

		StringBuilder textoSB = new StringBuilder(texto);
		StringBuilder claveSB = new StringBuilder(clave);
	
		StringBuilder encriptado = new StringBuilder();
		for (int i = 0; i < textoSB.length(); i++) {
			Character caracter = (char) (textoSB.charAt(i) ^ claveSB.charAt(i % claveSB.length()));
			
			textoSB.replace(i, i+1, caracter.toString());
		}
		
		return textoSB.toString();
	}

}
