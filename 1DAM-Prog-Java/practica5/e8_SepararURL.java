
/** e8_SepararURL.java
 *  Programa que separa las partes de una URL.
 *  mfp - 2019.01.11
 */

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class e8_SepararURL {

	public static void main(String[] args) {

		String url = "http://www.devbg.org/forum/index.php";

		String[] urlSeparada = separarURL(url);

		for (int i = 0; i < urlSeparada.length; i++) {
			System.out.println("Parte " + i + ": " + urlSeparada[i]);
		}

	}

	private static String[] separarURL(String url) {

		Pattern regex = Pattern.compile("(.+)(?:://)(.+?)(/.*)");
		Matcher resultado = regex.matcher(url);

		String[] respuesta = new String[3];

		if (resultado.find()) {
			respuesta = new String[resultado.groupCount()];
			for (int i = 0; i < respuesta.length; i++) {
				respuesta[i] = resultado.group(i + 1);
			}
		} else {
			for (int i = 0; i < respuesta.length; i++) {
				respuesta[i] = "No hay resultado.";
			}
		}

		return respuesta;
	}

}
