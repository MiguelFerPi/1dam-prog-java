import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class PruebasRegex {

	static public void main(String[] args) {
		String texto = "012abc"; // Ejemplo
		String regex = "\\d"; // Ejemplo

		Pattern patron = Pattern.compile(regex);
		Matcher concordancias = patron.matcher(texto);

		System.out.print("Posiciones:\t");
		for (int x = 0; x < texto.length(); x++) {
			if (x < 10) {
				System.out.print(x + "  ");
			} else {
				System.out.print(x + " ");
			}
		}

		System.out.print("\nTexto:\t\t");
		for (char c : texto.toCharArray()) {
			System.out.print(c + "  ");
		}

		System.out.println("\nRegex: " + concordancias.pattern());

		System.out.println("\nConcordancias: ");
		while (concordancias.find()) {
			System.out.println("[" + concordancias.start() + "," + concordancias.end() + "]: " + concordancias.group());
		}
	}
}
