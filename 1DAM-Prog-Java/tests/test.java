import java.util.Scanner;

public class test {
	public static void main(String[] args) {
		
		System.out.println("Escribe un texto");
		System.out.println("Pulsa intro 2 veces para terminar");
		System.out.println("======================");
		
		String texto = "";
		do {
			texto += new Scanner(System.in).nextLine() + "\n";
		} while(!texto.endsWith("\n\n"));
		texto = texto.trim();
		
		System.out.println("======================\n");
		System.out.println("Has escrito:");
		System.out.println("======================\n");
		System.out.println(texto);
		System.out.println("\n======================");
		
	}
}
