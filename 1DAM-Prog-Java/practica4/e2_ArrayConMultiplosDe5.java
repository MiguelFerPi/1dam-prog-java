/** e2_ArrayConMultiplosDe5.java
 *  Creación de un array de 20 elementos que contendrá multiplos de 5.
 *  mfp - 2018.11.21
 */

public class e2_ArrayConMultiplosDe5 {

	public static void main(String[] args) {

		int[] multiplosDe5 = new int[20];
		
		for (int i = 0; i < multiplosDe5.length; i++) {
			multiplosDe5[i] = i*5;
		}
		
		for (int i = 0; i < multiplosDe5.length; i++) {
			System.out.println(multiplosDe5[i]);
		}
		
	}

}

