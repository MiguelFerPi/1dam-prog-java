import java.util.Scanner;

/** e3_FrecuenciaNumero.java
 *  Dado un vector con números, devuelve la frecuencia de un valor dado.
 *  mfp - 2018.11.22
 */

public class e3_FrecuenciaNumero {

	public static void main(String[] args) {

		System.out.print("Introduzca la longitud del vector a usar: ");
		int vecLong = Integer.valueOf(new Scanner(System.in).nextLine());
		System.out.println();
		
		System.out.print("Introduzca el número entre 1 y 10 para calcular su frecuencia: ");
		int numero = Integer.valueOf(new Scanner(System.in).nextLine());
		System.out.println();
		
		// Se crea un vector con tantos valores aleatorios de 1 a 10 como el usuario ha indicado.
		int[] vector = new int[vecLong];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = (int) (Math.random()*10)+1;
		}

		// Se muestra en pantalla el resumen de los datos que se van a utilizar.
		System.out.print("Vector: {");
		for (int i = 0; i < vector.length; i++) {
			System.out.print(vector[i]);
			if (i < vector.length-1) {
				System.out.print(",");
			}
		}
		System.out.print("}\n");
		System.out.println("Numero: " + numero);

		// Resultado
		System.out.println("El número " + numero + " tiene una frecuencia de: " + frecuenciaNumero(vector, numero));
		
		
	}

	// Calcula la frecuencia del número en el vector introducidos.
	private static int frecuenciaNumero(int[] vector, int numero) {

		int contador = 0;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] == numero) contador++;
		}
		
		return contador;
	}

}

