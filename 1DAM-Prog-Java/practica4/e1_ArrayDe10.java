/** e1_ArrayDe10.java
 *  Crea un array de 10 valores con valor 7 en cada uno.
 *  mfp - 2018.11.21
 */

public class e1_ArrayDe10 {

	public static void main(String[] args) {

		// Primer método de inicialización
		int[] array1 = {7,7,7,7,7,7,7,7,7,7};
		
		for (int i = 0; i < array1.length; i++) {
			System.out.println(array1[i]);
		}
		
		System.out.println("===========");

		// Segundo método de inicialización
		int[] array2 = new int[] {7,7,7,7,7,7,7,7,7,7};
		
		for (int i = 0; i < array2.length; i++) {
			System.out.println(array2[i]);
		}
			
		System.out.println("===========");

		// Tercer método de inicialización
		int[] array3 = new int[10];
		
		for (int i=0; i < array3.length; i++) { 
			array3[i] = 7;
		}    
		
		for (int i = 0; i < array3.length; i++) {
			System.out.println(array3[i]);
		}
		
	}

}

