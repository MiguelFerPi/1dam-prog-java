/** e4_VectoresIguales.java
 *  Programa que compara dos vectores de Enteros y nos dice si son iguales o no.
 *  mfp - 2018.11.22
 */

public class e4_VectoresIguales {

	public static void main(String[] args) {

		// Vectores a comparar.
		int[] vector1 = {1,2,3,4,5,6,7,8,9,0};
		int[] vector2 = {1,2,3,4,5,6,7,8,9,0};
		
		// Mostramos los valores de los vectores.
		System.out.print("Vector 1: {");
		for (int i = 0; i < vector1.length; i++) {
			System.out.print(vector1[i]);
			if (i < vector1.length-1) {
				System.out.print(",");
			}
		}
		System.out.print("}\n");
		
		System.out.print("Vector 2: {");
		for (int i = 0; i < vector2.length; i++) {
			System.out.print(vector2[i]);
			if (i < vector2.length-1) {
				System.out.print(",");
			}
		}
		System.out.print("}\n");

		// Comparamos si son iguales con el método creado.
		if (vectoresIntIguales(vector1, vector2)) {
			System.out.println("Son iguales.");
		} else {
			System.out.println("No son iguales.");
		}
		
		
	}

	// Comprueba si 2 vectores introducidos sin iguales o no usando descarte de diferencias.
	private static boolean vectoresIntIguales(int[] vector1, int[] vector2) {

		if (vector1.length != vector2.length) return false;

		for (int i = 0; i < vector1.length; i++) {
			if (vector1[i] != vector2[i]) return false;
		}
		
		return true;

	}

}

