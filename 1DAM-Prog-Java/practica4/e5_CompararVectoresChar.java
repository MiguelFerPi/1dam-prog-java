/** e5_CompararVectoresChar.java
 *  Programa que compara dos vectores de caracteres y nos dice cual es el mayor o si son iguales.
 *  mfp - 2018.11.22
 */

public class e5_CompararVectoresChar {

	public static void main(String[] args) {

		// Vectores a comparar.
		char[] vector1 = {'a','b','c','d','e','f','g'};
		char[] vector2 = {'a','b','c','d','e','f','g'};
		
		// Mostramos los valores de los vectores.
		System.out.print("Vector 1: {");
		for (int i = 0; i < vector1.length; i++) {
			System.out.print(vector1[i]);
			if (i < vector1.length-1) {
				System.out.print(",");
			}
		}
		System.out.print("}\n");
		
		System.out.print("Vector 2: {");
		for (int i = 0; i < vector2.length; i++) {
			System.out.print(vector2[i]);
			if (i < vector2.length-1) {
				System.out.print(",");
			}
		}
		System.out.print("}\n");

		// Comparamos si son iguales con el método creado y seleccionamos el resultado correcto.
		switch (compararVectoresChar(vector1, vector2)) {
			case 0:
				System.out.println("Son iguales.");
				break;
				
			case 1:
				System.out.println("Vector1 es mayor.");
				break;
				
			case -1:
				System.out.println("Vector2 es mayor.");
				break;

			default:
				System.out.println("Los vectores no se pueden comparar.");
		}
		
		
	}

	// Compara el contenido de dos vectores de caracteres introducidos y devuelve cual es mayor.
	private static int compararVectoresChar(char[] vector1, char[] vector2) {

		// Retorno en caso de que los vectores no tengan el mismo número de valores.
		if (vector1.length != vector2.length) return -2;
		
		// Calculamos la diferencia entre vectores, comparado elemento por elemento. 
		int diferencia = 0;
		for (int i = 0; i < vector1.length; i++) {
			if (vector1[i] > vector2[i]) {
				diferencia++;
			} else if (vector1[i] < vector2[i]) {
				diferencia--;
			}
		}
		
		return diferencia;

	}

}

