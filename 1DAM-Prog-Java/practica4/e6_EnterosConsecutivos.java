/** e6_EnterosConsecutivos.java
 *  Programa que comprueba cuantos números enteros consecutivos hay en un vector.
 *  mfp - 2018.11.22
 */

import java.util.Scanner;

public class e6_EnterosConsecutivos {

	public static void main(String[] args) {

		System.out.print("Introduzca la longitud del vector a usar: ");
		int vecLong = Integer.valueOf(new Scanner(System.in).nextLine());
		System.out.println();
		
		// Se crea un vector con tantos valores aleatorios de 1 a 10 como el usuario ha indicado.
		int[] vector = new int[vecLong];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = (int) (Math.random()*10)+1;
		}

		// Se muestra en pantalla el resumen de los datos que se van a utilizar.
		System.out.print("Vector:\n{");
		for (int i = 0; i < vector.length; i++) {
			System.out.print(vector[i]);
			if (i < vector.length-1) {
				System.out.print(",");
			}
			if (i > 1 && i % 15 == 0) {
				System.out.println();
			}
		}
		System.out.print("}\n\n");
		
		int consecutivos = maximoIntConsecutivos(vector);
		if (consecutivos == 1) {
			System.out.println("No hay números consecutivos.");
		} else {
			System.out.println("Hay " + consecutivos + " números consecutivos.");
		}
		
	}

	// Cuenta los números consecutivos en el vector dado.
	private static int maximoIntConsecutivos(int[] vector) {

		int consecutivos = 1;
		
		for (int i = 1; i < vector.length; i++) {
			if (vector[i-1] + 1 == vector[i]) {
				consecutivos++;
				//System.out.println("i " + i + ": " + vector[i-1] + "-" + vector[i]); // Muestra cada pareja de consecutivos.
			}
		}
		
		return consecutivos;
	}

}

