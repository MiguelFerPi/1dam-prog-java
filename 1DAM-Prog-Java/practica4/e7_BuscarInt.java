/** e7_BuscarInt.java
 *  Programa que busca un número en un vector.
 *  mfp - 2018.11.24
 */

public class e7_BuscarInt {

	public static void main(String[] args) {

		int[] vectorNumeros = {9,8,7,6,5,4,3,2,1,0};
		int numeroBuscar = 3;
		
		
		
		System.out.print("Vector introducido: {");
		for (int i = 0; i < vectorNumeros.length; i++) {
			System.out.print(vectorNumeros[i]);
			if (i < vectorNumeros.length-1) {
				System.out.print(",");
			}
		}
		System.out.print("}\n");
		System.out.println("Número a buscar: " + numeroBuscar);
		System.out.println();
		
		for (int i = 0; i < vectorNumeros.length; i++) {
			if (vectorNumeros[i] == numeroBuscar) {
				System.out.println("El número buscado está en el índice " + i + ".");
				break;
			}
		}
	}

}
